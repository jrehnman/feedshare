
from schema import Schema


rssSchema = \
    Schema('feedMetaData', xpath='channel', required=True) \
        .synthetic('feed') \
        .textprop('title') \
        .textprop('link') \
        .textprop('description') \
        .textprop('language') \
        .textprop('copyright') \
        .textprop('managingEditor') \
        .textprop('webMaster') \
        .textprop('generator') \
        .textprop('docs') \
        .textprop('rating') \
        .textprop('ttl') \
        .textprop('pubDate') \
        .textprop('lastBuildDate') \
        .synthetic('pModified') \
        .synthetic('pVersionAge') \
        .child('feedImages', xpath='image') \
            .synthetic('feed') \
            .textprop('url') \
            .textprop('title') \
            .textprop('link') \
            .textprop('description') \
            .textprop('width') \
            .textprop('height') \
        .end() \
        .child('clouds', xpath='cloud') \
            .synthetic('feed') \
            .attribute('domain') \
            .attribute('port') \
            .attribute('path') \
            .attribute('registerProcedure') \
            .attribute('protocol') \
        .end() \
        .child('feedCategories', xpath='category', many=True) \
            .synthetic('feed') \
            .prop('category', xpath='text()') \
            .attribute('domain') \
        .end() \
        .child('skipHours') \
            .synthetic('feed') \
            .textprop('hour') \
        .end() \
        .child('skipDays') \
            .synthetic('feed') \
            .textprop('day') \
        .end() \
        .child('items', xpath='item', many=True) \
            .namespace(content='http://purl.org/rss/1.0/modules/content/') \
            .synthetic('feed') \
            .textprop('title') \
            .textprop('description') \
            .prop('encoded', xpath='content:encoded/text()') \
            .textprop('link') \
            .textprop('author') \
            .textprop('comments') \
            .textprop('guid') \
            .prop('guidIsPermaLink', xpath='guid/@isPermaLink') \
            .textprop('pubDate') \
            .prop('sourceUrl', xpath='source/@url') \
            .prop('sourceTitle', xpath='source/text()') \
            .synthetic('pUuid') \
            .synthetic('pDiscovered') \
            .child('enclosures', xpath='enclosure') \
                .synthetic('uuid') \
                .attribute('url') \
                .attribute('length') \
                .attribute('type') \
            .end() \
            .child('itemCategories', xpath='category', many=True) \
                .synthetic('uuid') \
                .prop('category', xpath='text()') \
                .attribute('domain') \
            .end() \
        .end()

