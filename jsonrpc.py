
import datetime
import json
import sys
import traceback
import uuid


class JsonRpcError(Exception):
    def __init__(self, code, message):
        Exception.__init__(self)
        self.code = code
        self.message = message


def jsonrpc(jsonstr, jsonobj):
    request = None
    try:
        request = json.loads(jsonstr)
    except ValueError:
        return json_parse_error()

    jsonid = request.get('id')
    if not json_request_is_valid(request):
        # Something's missing from request
        return json_invalid_request(jsonid)
    elif not hasattr(jsonobj, 'json_' + request['method']):
        # Request method not found
        return json_unknown_method(jsonid)
    else:
        # Request OK
        methodname = request['method']
        params = request.get('params', [])
        method = getattr(jsonobj, 'json_' + methodname)

        try:
            result = method(*params) if isinstance(params, list) \
                else method(**params)

            return json_response(result, jsonid)
        except TypeError:
            excinfo = sys.exc_info()
            traceback.print_exception(*excinfo)
            return json_invalid_params(jsonid)
        except JsonRpcError as e:
            return json_error(e.code, e.message, jsonid)
        except Exception as e:
            excinfo = sys.exc_info()
            traceback.print_exception(*excinfo)
            return json_internal_error(e.message, jsonid)


def extended_json_transform(o):
    if isinstance(o, datetime.datetime):
        return o.isoformat()
    elif isinstance(o, uuid.UUID):
        return str(o)
    elif isinstance(o, set):
        return list(o)
    else:
        raise TypeError(str(type(o)))


def json_request_is_valid(request):
    if isinstance(request, dict):
        jsonrpcversion = request.get('jsonrpc')
        if ('method' in request) and (jsonrpcversion == '2.0'):
            if 'params' in request:
                paramsislist = isinstance(request['params'], list)
                paramsisdict = isinstance(request['params'], dict)
                return paramsislist or paramsisdict
            else:
                return True
        else:
            return False
    else:
        return False

def json_request_is_notification(request):
    return not 'id' in request

def json_message(result, error, jsonid):
    return \
        { 'jsonrpc': '2.0'
        , 'result': result
        , 'error': error
        , 'id': jsonid
        }

def json_response(result, jsonid):
    return json_message(result, None, jsonid)

def json_error(code, message, jsonid = None, data = None):
    error = {'code': code, 'message': message, 'data': data}
    return json_message(None, error, jsonid)

def json_parse_error():
    return json_error(e_parse_error, 'Parse error')

def json_invalid_request(jsonid):
    return json_error(e_invalid_request, 'Invalid JSON-RPC', jsonid)

def json_unknown_method(jsonid):
    return json_error(e_method_not_found, 'Unknown method', jsonid)

def json_invalid_params(jsonid):
    return json_error(e_invalid_params, 'Invalid method parameter(s)', jsonid)

def json_internal_error(message, jsonid):
    return json_error(
        e_internal_error, 
        'Internal JSON-RPC error: ' + message, 
        jsonid)


e_parse_error = -32700
e_invalid_request = -32600
e_method_not_found = -32601
e_invalid_params = -32602
e_internal_error = -32603

