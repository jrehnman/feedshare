# README #

### Prerequisites ###
* [python-openid](https://pypi.python.org/pypi/python-openid/)
* [pyscopg2](https://pypi.python.org/pypi/psycopg2)
* [pycurl](https://pypi.python.org/pypi/pycurl)
* [lxml](https://pypi.python.org/pypi/lxml)
* (Optional) [coverage](https://pypi.python.org/pypi/coverage)
* (Optional) [pylint](https://pypi.python.org/pypi/pylint)
* PostgreSQL

### Setting up database ###
* Create a user and an UTF8 encoded database in PostgreSQL. I.e, in an SQL shell
```
#!SQL
CREATE ROLE feedshare;
CREATE DATABASE feedshare WITH OWNER=feedshare ENCODING=UTF8;
```

* feedshare does not send a password with its SQL connection string, it assumes that the feedshare PostgreSQL user is set up to use "*peer*" method of authentication in *pg_hba.conf*
* Run the scripts in the feedsare SQL/ directory in sequence, starting with *create.sql* followed by the *upgrade* scripts:
```
#!sh
for sql in create.sql upgrade1to2.sql upgrade2to3.sql upgrade3to4.sql; do psql --username=feedshare --dbname=feedshare --file=$sql; done;
```

### Configuration ###
Both the server for the web app and the feed scraper can be forced to write their default configuration to file for further customization:
```
#!sh
python server.py --write-default-config
python rss.py --write-default config
```
#### Generating SSL certificate ####
The server communicates only over HTTPS by default and therefore needs a SSL certificate. A self-signed certificate for development can be generated using:
```
#!sh
openssl req -new -outform PEM -x509 -nodes -days 365 -pubkey -out server.pem -keyout server.pem
```
