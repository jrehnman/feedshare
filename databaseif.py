
from rssschema import rssSchema
from utils import intersperse


def userexists(c, openid):
    query = "SELECT COUNT(*) > 0 FROM subscribers WHERE openid=%s;"
    c.execute(query, (openid, ))
    (exists,) = c.fetchone()
    return exists

def adduser(c, openid):
    if not userexists(c, openid):
        query = "INSERT INTO subscribers (openid) VALUES (%s);"
        c.execute(query, (openid, ))
        return True
    else:
        return False

def userisadmin(c, openid):
    if userexists(c, openid):
        return False # TODO Add admin status to user table
    else:
        return False

def feeddataexists(c, url):
    query = "SELECT COUNT(*) > 0 \
        FROM feedMetaData \
        WHERE feed=%s;"
    c.execute(query, (url, ))
    (exists,) = c.fetchone()
    return exists

def getsubscriptions(c, openid):
    columns = [p.name for p in rssSchema.properties]

    query = \
        "WITH unreadCount AS ( \
            SELECT \
                feedcounts.feed AS feed, \
                feedcounts.count - COALESCE(readfeedcounts.count, 0) AS unread \
            FROM feedcounts \
            LEFT JOIN readfeedcounts \
            ON  readfeedcounts.feed=feedcounts.feed AND \
                readfeedcounts.subscriber = %s ) \
        SELECT \
            {0}, unreadCount.unread \
        FROM feedMetaData \
        INNER JOIN subscriptions ON \
            feedMetaData.feed = subscriptions.feed AND \
            feedMetaData.pVersionAge = 0 \
        LEFT JOIN unreadCount ON \
            subscriptions.feed = unreadCount.feed \
        WHERE \
            subscriptions.subscriber=%s".format(
            intersperse(['feedMetaData.' + column for column in columns], ","))

    c.execute(query, (openid, openid))
    result = dictifyrow(columns + ['unread'], c)

    for row in result:
        row['tags'] = getuserfeedtags(c, openid, row['feed'])

    return result

def feedexists(c, rssurl):
    query = "SELECT COUNT(*) > 0 FROM feeds WHERE url=%s;"
    c.execute(query, (rssurl, ))
    (exists,) = c.fetchone()
    return exists

def registerfeed(c, rssurl):
    if not feedexists(c, rssurl):
        query = "INSERT INTO feeds (url) VALUES (%s);"
        c.execute(query, (rssurl, ))

def hassubscription(c, openid, rssurl):
    query = "SELECT COUNT(*) > 0 \
        FROM subscriptions \
        WHERE subscriber=%s AND feed=%s;"
    c.execute(query, (openid, rssurl))
    (exists, ) = c.fetchone()
    return exists

def subscribe(c, openid, rssurl):
    registerfeed(c, rssurl)
    if not hassubscription(c, openid, rssurl):
        query = "INSERT INTO subscriptions (subscriber, feed) VALUES (%s, %s);"
        c.execute(query, (openid, rssurl))
        return True
    else:
        return False

def unsubscribe(c, openid, feedurl):
    query = "DELETE FROM subscriptions WHERE subscriber=%s AND feed=%s;"
    c.execute(query, (openid, feedurl))

def getstoredhash(cursor, url):
    sql = 'SELECT hash FROM feeds WHERE url=%s;'
    cursor.execute(sql, (url, ))
    row = cursor.fetchone()
    return row[0] if row else None

def storehash(cursor, url, hashval):
    if feedexists(cursor, url):
        sql = 'UPDATE feeds SET hash=%s WHERE url=%s;'
        cursor.execute(sql, (hashval, url))

def getsentinel(c):
    query = 'SELECT MAX(pSentinel) FROM items;'
    c.execute(query)
    (sentinel, ) = c.fetchone()
    return sentinel

def feedunreadcount(c, openid, feed):
    query = ' \
        SELECT (feedcounts.count - COALESCE(readfeedcounts.count, 0)) \
            FROM feedcounts \
        LEFT JOIN readfeedcounts ON \
            readfeedcounts.feed         = feedcounts.feed AND \
            readfeedcounts.subscriber   = %s \
        WHERE \
            feedcounts.feed=%s;'

    c.execute(query, (openid, feed))
    (unread, ) = c.fetchone()
    return unread

def tagunreadcount(c, tagowner, tag, subscriber):
    query = ' \
        SELECT unread \
        FROM tagunreadcount \
        WHERE \
            owner                       = %s AND \
            tag                         = %s AND \
            subscriber                  = %s;'

    c.execute(query, (tagowner, tag, subscriber))
    (unread, ) = c.fetchone()
    return unread

def feeditems(c, openid, feed, first, last, onlyunread, ascending, refsentinel):
    columns = [p.name for p in rssSchema.children['items'].properties]
    query = ' \
        SELECT {0}, enclosures.url, enclosures.type, read.uuid IS NOT NULL \
        FROM items \
        LEFT JOIN enclosures ON \
            items.pUuid = enclosures.uuid \
        LEFT JOIN read ON \
            items.pUuid = read.uuid AND \
            read.subscriber = %s \
        WHERE \
            items.pSentinel <= %s AND \
            feed = %s AND \
            (%s OR read.uuid IS NULL) \
        ORDER BY COALESCE(pubDate, pDiscovered) {1}, pDiscovered {1} \
        LIMIT %s OFFSET %s;'.format(
            intersperse(columns, ","),
            'ASC' if ascending else 'DESC')

    getall = not onlyunread
    limit = last - first if getall \
        else min(last - first, max(feedunreadcount(c, openid, feed) - first, 0))

    c.execute(query, (openid, refsentinel, feed, getall, limit, first))
    result = dictifyrow(columns + ['enclosure-url', 'enclosure-type', 'read'], c)

    for row in result:
        row['tags'] = getuseritemtags(c, openid, row['pUuid'])

    return result

def taggeditems(c, tagowner, tag, subscriber, first, last, onlyunread, ascending, refsentinel):
    columns = [p.name for p in rssSchema.children['items'].properties]

    query = ' \
        SELECT {0}, enclosures.url, enclosures.type, read.uuid IS NOT NULL \
        FROM items \
        LEFT JOIN enclosures ON \
            items.pUuid     = enclosures.uuid \
        LEFT JOIN read ON \
            items.pUuid     = read.uuid AND \
            read.subscriber = %s \
        WHERE \
            (items.feed in \
                (SELECT feed \
                FROM feedtags \
                WHERE \
                    subscriber=%s \
                    AND tag=%s) OR \
            items.pUuid in \
                (SELECT uuid \
                from itemtags \
                WHERE \
                    subscriber=%s \
                    AND tag=%s)) AND \
            (%s OR read.uuid IS NULL) AND \
            (items.pSentinel <= %s) \
        ORDER BY COALESCE(items.pubdate, items.pDiscovered) {1}, items.pDiscovered {1} \
        LIMIT %s OFFSET %s;'.format(
            intersperse(['items.' + column for column in columns], ","),
            "ASC" if ascending else "DESC")

    getall = not onlyunread
    limit = last - first if getall \
        else min(last - first, max(tagunreadcount(c, tagowner, tag, subscriber) - first, 0))

    c.execute(query, (subscriber, tagowner, tag, tagowner, tag, getall, refsentinel, limit, first))
    result = dictifyrow(columns + ['enclosure-url', 'enclosure-type', 'read'], c)

    for row in result:
        row['tags'] = getuseritemtags(c, subscriber, row['pUuid'])

    return result

def tagexists(c, tag, openid):
    query = "SELECT COUNT(*) > 0 \
        FROM tags \
        WHERE tag=%s AND subscriber=%s;"
    c.execute(query, (tag, openid))
    (exists, ) = c.fetchone()
    return exists

def addtag(c, tag, openid):
    if not tagexists(c, tag, openid):
        query = "INSERT INTO tags (tag, subscriber) VALUES (%s, %s);"
        c.execute(query, (tag, openid))
        return True
    else:
        return False

def itemtagexists(c, tag, itemid, openid):
    query = "SELECT COUNT(*) > 0 \
        FROM taggedItems \
        WHERE tag=%s AND uuid=%s;"
    c.execute(query, (tag, itemid))
    (exists, ) = c.fetchone()
    return exists

def tagitem(c, openid, itemid, tags):
    appliedtags = []
    for tag in set(tags):
        addtag(c, tag, openid)

        # TODO Check if postid exists (?)
        if not itemtagexists(c, tag, itemid, openid):
            query = "INSERT INTO itemTags (tag, subscriber, uuid) \
                VALUES (%s, %s, %s);"
            c.execute(query, (tag, openid, itemid))
            appliedtags += tag

    return list(set(tags) - set(appliedtags))

def getuseritemtags(c, openid, itemid):
    query = 'SELECT tag \
        FROM taggedItems \
        WHERE subscriber = %s AND uuid=%s;'
    c.execute(query, (openid, itemid))
    return list(map(lambda row: row[0], c))

def getallitemtags(c, itemid):
    query = 'SELECT tag, COUNT(tag) \
        FROM taggedItems \
        WHERE uuid = %s \
        GROUP BY tag;'
    c.execute(query, (itemid, ))
    return list(map(lambda row: row[0], c))

def getuserfeedtags(c, openid, feedurl):
    query = "SELECT tag \
        FROM feedTags \
        WHERE subscriber=%s AND feed=%s;"
    c.execute(query, (openid, feedurl))
    return [row[0] for row in c]

def getallfeedtags(c, feedurl):
    query = "SELECT tag \
        FROM feedTags \
        WHERE feed=%s;"
    c.execute(query, (feedurl, ))
    return [row[0] for row in c]

def feedtagexists(c, openid, feedurl, tag):
    query = "SELECT COUNT(*) > 0 \
        FROM feedTags \
        WHERE subscriber=%s AND feed=%s AND tag=%s;"
    c.execute(query, (openid, feedurl, tag))
    (exists, ) = c.fetchone()
    return exists

def tagfeed(c, openid, feedurl, tags):
    appliedtags = []
    for tag in tags:
        addtag(c, tag, openid)

        # TODO Check if user is subscribed to feed
        if not feedtagexists(c, openid, feedurl, tag):
            query = " \
                INSERT INTO feedTags (tag, subscriber, feed) \
                VALUES (%s, %s, %s);"
            c.execute(query, (tag, openid, feedurl))
            appliedtags.append(tag)

    return list(set(tags) - set(appliedtags))

def untagfeed(c, openid, feedurl, tag):
    query = "DELETE FROM feedTags WHERE tag=%s AND subscriber=%s AND feed=%s;"
    c.execute(query, (tag, openid, feedurl))

def getsharedtags(c, usersharedto):
    query = '\
        SELECT \
            shares.tag, \
            shares.subscriber, \
            subscribers.friendlyname, \
            tagunreadcount.unread \
        FROM shares \
        LEFT JOIN subscribers ON \
            shares.subscriber       = subscribers.openid \
        LEFT JOIN tagunreadcount ON \
            shares.subscriber       = tagunreadcount.owner AND \
            shares.tag              = tagunreadcount.tag AND \
            shares.sharedwith       = tagunreadcount.subscriber \
        WHERE \
            shares.sharedwith       = %s;'

    c.execute(query, (usersharedto, ))
    result = dictifyrow(['tag', 'sharer', 'friendlyname', 'unread'], c)

    return result

def getallusertags(c, openid):
    query = '\
        SELECT tag, unread \
        FROM tagunreadcount \
        WHERE subscriber=%s;'

    c.execute(query, (openid,))
    result = dictifyrow(['tag', 'unread'], c)

    for row in result:
        row['sharedwith'] = gettagsharedwith(c, openid, row['tag'])

    return result

def gettagsharedwith(c, openid, tag):
    query = '\
        SELECT sharedwith \
        FROM shares \
        WHERE subscriber=%s AND tag=%s;'

    c.execute(query, (openid, tag))
    return [sharedwith for (sharedwith, ) in c]

def sharetag(c, openid, tag, sharedwith):
    if not tagexists(c, tag, openid):
        raise NoSuchTagError()
    if sharedwith in gettagsharedwith(c, openid, tag):
        raise AlreadySharedError()
    if not userexists(c, sharedwith):
        raise NoSuchUserError()

    query = 'INSERT INTO shares (subscriber, tag, sharedwith) VALUES (%s, %s, %s);'
    c.execute(query, (openid, tag, sharedwith))

def unsharetag(c, openid, tag, sharedwith):
    query = 'DELETE FROM shares WHERE subscriber=%s AND tag=%s AND sharedwith=%s;'
    c.execute(query, (openid, tag, sharedwith))

def deletetag(c, openid, tag):
    query = 'DELETE FROM tags WHERE subscriber=%s AND tag=%s;'
    c.execute(query, (openid, tag))
    # TODO delete comments (?)

def itemexists(c, itemid):
    query = "SELECT COUNT(*) > 0 \
        FROM items \
        WHERE pUuid=%s;"
    c.execute(query, (itemid, ))
    (exists, ) = c.fetchone()
    return exists

def isitemread(c, openid, itemid):
    query = "SELECT COUNT(*) > 0 \
        FROM read \
        WHERE subscriber=%s AND uuid=%s;"
    c.execute(query, (openid, itemid))
    (isread, ) = c.fetchone()
    return isread

def markitemread(c, openid, itemid):
    if not itemexists(c, itemid):
        raise NoSuchItemError()

    if not isitemread(c, openid, itemid):
        query = "INSERT INTO read (subscriber, uuid) VALUES (%s, %s);"
        c.execute(query, (openid, itemid))
        return True
    else:
        return False

def markfeedread(c, openid, feedurl, refsentinel):
    query = 'INSERT INTO read (subscriber, uuid) \
        SELECT %s AS subscriber, items.pUuid AS uuid \
        FROM items \
        WHERE \
        items.feed = %s AND \
        NOT EXISTS (SELECT TRUE FROM read WHERE subscriber=%s AND uuid=items.pUuid) AND \
        items.pSentinel <= %s';

    c.execute(query, (openid, feedurl, openid, refsentinel))

def marktagread(c, user, tag, tagowner, refsentinel):
    query = 'INSERT INTO read (subscriber, uuid) \
        SELECT %s AS subscriber, items.pUuid AS uuid \
        FROM items \
        WHERE \
        ( items.feed IN (SELECT feed FROM feedtags WHERE tag=%s AND subscriber=%s) OR \
        items.pUuid IN (SELECT uuid FROM itemtags WHERE tag=%s AND subscriber=%s) ) AND \
        NOT EXISTS (SELECT TRUE FROM read WHERE subscriber=%s AND uuid=items.pUuid) AND \
        items.pSentinel <= %s;'

    c.execute(query, (user, tag, tagowner, tag, tagowner, user, refsentinel))

def markitemunread(c, openid, itemid):
    query = "DELETE FROM read WHERE subscriber=%s AND uuid=%s;"
    c.execute(query, (openid, itemid))

def purgefeed(c, feed):
    query = '\
        /* Disable triggers to speed up operation */ \
        ALTER TABLE items \
            DISABLE TRIGGER trigger_update_feedcounts; \
        ALTER TABLE read \
            DISABLE TRIGGER trigger_update_readfeedcounts; \
        \
        /* Cascade delete feed */ \
        DELETE FROM feeds \
            WHERE url = %s; \
        \
        /* Re-enable triggers */ \
        ALTER TABLE items \
            ENABLE TRIGGER trigger_update_feedcounts; \
        ALTER TABLE read \
            ENABLE TRIGGER trigger_update_readfeedcounts;'

    c.execute(query, (feed, ))

def feedstats(c):
    query = '\
        WITH stats AS ( \
            SELECT feed, MAX(pDiscovered) AS latest, COUNT(*) AS posts, COUNT(guid) > 0 AS hasguid \
            FROM items \
            GROUP BY feed), \
        subscribercount AS ( \
            SELECT feed, count(*) as c \
            FROM subscriptions \
            GROUP BY feed) \
        SELECT feedmetadata.title, stats.feed, stats.latest, stats.posts, subscribercount.c, stats.hasguid \
            FROM stats \
            LEFT JOIN subscribercount ON \
                stats.feed = subscribercount.feed \
            LEFT JOIN feedmetadata ON \
                stats.feed = feedmetadata.feed AND feedmetadata.pVersionAge = 0;'

    c.execute(query)
    result = dictifyrow(['title', 'feed', 'latest', 'posts', 'subscribers', 'guid'], c)
    return result

def userstats(c):
    query = 'SELECT openid, friendlyname FROM subscribers;'
    c.execute(query)
    result = dictifyrow(['openid', 'friendlyname'], c)
    return result

def dictifyrow(columns, rows):
    return [dict(zip(columns, row)) for row in rows]

class ReaderInterfaceError(Exception):
    def __init__(self, reason):
        Exception.__init__(self, reason)

class NoSuchTagError(ReaderInterfaceError):
    def __init__(self):
        ReaderInterfaceError.__init__(self, 'No Such tag')

class NoSuchItemError(ReaderInterfaceError):
    def __init__(self):
        ReaderInterfaceError.__init__(self, 'No such feed item')

class AlreadySharedError(ReaderInterfaceError):
    def __init__(self):
        ReaderInterfaceError.__init__(self, 'Tag already shared with user')

class NoSuchUserError(ReaderInterfaceError):
    def __init__(self):
        ReaderInterfaceError.__init__(self, 'No such user')

