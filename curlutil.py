
import os
import pycurl
import re
import urllib.parse


encoding_pattern = re.compile('^Content-Type:.*;.*charset=([^;]+)', re.I)
filename_pattern = re.compile('^Content-Disposition:.*filename="([^"]*)".*$', re.I)

class CurlRead:
    def __init__(self):
        self.content = b''

    def feed(self, data):
        self.content += data

class CurlReadHeader:
    def __init__(self, patterns):
        self.patterns = patterns
        self.matches = {}
        for name in patterns:
            self.matches[name] = None

    def read(self, hdrline):
        for name, pat in self.patterns.items():
            match = pat.match(hdrline)
            if match:
                self.patterns[p] = match

def curlget(url, headercb=None, useragent=None, follow=True):
    r = CurlRead()
    c = pycurl.Curl()
    c.setopt(pycurl.URL, url)
    c.setopt(pycurl.WRITEFUNCTION, r.feed)
    c.setopt(pycurl.ENCODING, 'identity, gzip, deflate')
    c.setopt(pycurl.TIMEOUT, 10*60)
    if follow:
        c.setopt(pycurl.FOLLOWLOCATION, 1)
    if headercb is not None:
        c.setopt(pycurl.HEADERFUNCTION, headercb)
    if useragent is not None:
        c.setopt(pycurl.USERAGENT, useragent)
    c.setopt(pycurl.HTTPHEADER, ['Pragma: no-cache'])
    c.setopt(pycurl.HTTPHEADER, ['Cache-Control: no-cache'])
    c.perform()
    c.close()
    return r.content

def download(url, path):
    headers = CurlReadHeader({'filename': filename_pattern})
    content = curlget(url, headers.read)
    match = headers.matches['filename']

    filename = None
    if not match:
        filename = filenamefromurl(url)
        if filename == '':
            filename = url
    else:
        filename = match.group(1)

    fullpath = os.path.join(path, filename)
    with open(fullpath, 'wb') as f:
        f.write(content)

def filenamefromurl(url):
    urltuple = urllib.parse.urlsplit(url)
    return urltuple.path.split('/')[-1]


