
import argparse
import json
import os.path


def read_json_config(defaultconfig, configfilename):
    if os.path.exists(configfilename):
        try:
            cf = open(configfilename, 'r')
            userconfig = json.load(cf)
            
            userkeys = set(userconfig.keys())
            defaultkeys = set(defaultconfig.keys())

            unknownkeys = userkeys - defaultkeys
            unspecifiedkeys = defaultkeys - userkeys
    
            defaultconfig.update(userconfig)

            return (True, unknownkeys, unspecifiedkeys)
        finally:
            cf.close()

    else:
        return (False, set(), set(defaultconfig.keys()))

def write_json_config(config, configfilename):
    try:
        cf = file(configfilename, 'w')
        json.dump(config, cf, indent=3)
    finally:
        cf.close()

def config_argsparser(defaultconfigfile):
    parser = argparse.ArgumentParser()
    parser.add_argument('--config-file', 
        type=str, 
        default=defaultconfigfile, 
        help='Configuration file name')
    parser.add_argument('--write-default-config', 
        action='store_true', 
        help='Writes default configuration to file and quits')
    return parser

def write_default_config(defaultconfig, configfilename):
    if not os.path.exists(configfilename):
        print('Writing default configuration to {0}'.format(configfilename))
        write_json_config(defaultconfig, configfilename)
    else:
        print('Configuration file "{0}" already exists. Use --config-file to specify another file'.format(configfilename))

