
import json
import unittest
import os
import os.path
import scraper


def path2url(path):
    fullpath = os.path.abspath(path)
    return 'file://' + fullpath

def mkfeedtest(url, result_path):
    def testfn(tc):
        feed_hash, feed = scraper.parsefeed(url, None)
        with open(result_path) as result_file:
            expected = json.load(result_file)
            tc.assertEqual(feed, expected)

    return testfn

class TestScraper(unittest.TestCase):
    pass


testdata = \
    [ ('atom', path2url('ut/dilbert_atom.xml'), 'ut/dilbert_atom.json')
    , ('rss', path2url('ut/freebsd_rss.xml'), 'ut/freebsd_rss.json')
    , ('encoded', path2url('ut/365tomorrows_rss.xml'), 'ut/365tomorrows_rss.json')
    ]

# This could easier be achieved by using unittest.TestCase.subTest, but
# that is only available from Python 3.4
for name, url, result_path in testdata:
    setattr(TestScraper, 'test_feed_' + name, mkfeedtest(url, result_path))


if __name__ == '__main__':
    unittest.main()

