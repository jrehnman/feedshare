
import psycopg2
import psycopg2.extras

def connect(database, user, options=''):
    psycopg2.extras.register_uuid()
    return psycopg2.connect('dbname={0} user={1} {2}'.format(database, user, options))

class WrappedCursor:
    def __init__(self, db):
        self.db = db
        self.cursor = None

    def __enter__(self):
        self.cursor = self.db.cursor()
        return self.cursor

    def __exit__(self, typ, value, traceback):
        if typ:
            # Exception occured
            self.db.rollback()
        else:
            self.db.commit()

        if self.cursor:
            self.cursor.close()

        return False # Dont' swallow exception if one occured, otherwise ignored

def create(connection):
    cursor = connection.cursor()
    execSqlScript(cursor, 'sql/create.sql')
    connection.commit()

def upgrade(connection):
    upgradescripts = \
        [ ('1', 'sql/upgrade1to2.sql')
        , ('2', 'sql/upgrade2to3.sql')
        ]

    for (expectedver, script) in upgradescripts:
        if databaseIsVersion(cursor, expectedver):
            execSqlScript(cursor, script)
            connection.commit()
        else:
            msg = 'DB version check failed before running {0}, expected {1}'
            raise msg.format(expectedver, script)

def databaseIsVersion(cursor, requiredVersion):
    sql = 'SELECT value=%s FROM metadata WHERE key=%s;'
    cursor.execute(sql, (str(requiredVersion), 'version'))
    (isRequiredVersion,) = cursor.fetchone()
    return isRequiredVersion

def execSqlScript(cursor, filename):
    try:
        f = file(filename, 'r')
        cursor.execute(f.read())
    finally:
        f.close()

