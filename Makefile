
PYTHON ?= python3
COVERAGE ?= coverage3

LINT ?= $(PYTHON) pylint
LINT_HTML ?= pylint.html
LINT_TARGETS ?= $(wildcard *.py)


.PHONY:all html coverage coverage_report coverage_html lint lint_html unittests

all: lint coverage_report 

html: lint_html coverage_html

coverage:
	PYTHONPATH=. $(COVERAGE) run --source . -m ut.test_scraper

coverage_report: coverage
	$(COVERAGE) report

coverage_html: coverage
	$(COVERAGE) html

lint:
	-$(LINT) --rcfile=.pylintrc $(LINT_TARGETS)

lint_html:
	-$(LINT) --rcfile=.pylintrc --output-format=html $(LINT_TARGETS) > $(LINT_HTML)

unittests:
	$(PYTHON) -m ut.test_scraper

