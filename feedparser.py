
from lxml import etree
import re
from utils import current_exception


parser = etree.XMLParser(recover=True)


class FeedError(Exception):
    def __init__(self, message):
        Exception.__init__(self, message)

class XmlParseError(FeedError):
    def __init__(self, message):
        FeedError.__init__(self, message)

class FeedFormatError(FeedError):
    def __init__(self, message, expr, line):
        FeedError.__init__(self, message)
        self.expr = expr
        self.line = line

def innerXML(xmltag):
    preamble = xmltag.text if xmltag.text else ''
    return preamble + ''.join(map(lambda x: etree.tostring(x, encoding='unicode'), xmltag.getchildren())) 

def parseproperties(xmltag, schema):
    result = {}
    for p in schema.properties:
        if not p.issynthetic:
            xpathresult = xmltag.xpath(p.xpath, namespaces=schema.namespaces)
            if xpathresult:
                if type(xpathresult[0]) == etree._Element:
                    result[p.name] = innerXML(xpathresult[0])
                else:
                    result[p.name] = xpathresult[0]
            else:
                result[p.name] = None

    return result

def parsechildren(xmltag, schema, log=None):
    result = {}
    for c in schema.children.values():
        result[c.name] = parsetag(xmltag, c, log)
    return result

def parsetag(xmlroot, schema, log=None):
    xmltag = xmlroot.xpath(schema.xpath, namespaces=schema.namespaces)
    if xmltag:
        if schema.many:
            result = []
            for x in xmltag:
                item = parseproperties(x, schema)
                item.update(parsechildren(x, schema, log))
                result.append(item)
            return result
        else:
            if len(xmltag) != 1:
                if log:
                    msg = 'Warning, wrong multiplicity for expression "{}" in context "{}"'
                    log(msg.format(schema.xpath, xmlroot.tag))

            result = parseproperties(xmltag[0], schema)
            result.update(parsechildren(xmltag[0], schema, log))
            return result
    else:
        if schema.required:
            msg = 'Missing requred tag for expression "{0}" in context "{1}"'.format(schema.xpath, xmlroot.tag)
            raise FeedFormatError(msg, schema.xpath, xmlroot.sourceline)
        if schema.many:
            return []
        else:
            return None

def parsexml(xmlsrc):
    xml = None
    try:
        xml = etree.fromstring(xmlsrc, parser)
    except:
        raise XmlParseError(current_exception())

    if xml is None:
        raise XmlParseError('No XML found')

    return xml

