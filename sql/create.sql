
CREATE TYPE weekday AS ENUM ('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
CREATE DOMAIN urlType AS varchar;
CREATE DOMAIN emailType AS varchar;

/*  Feeds table */
CREATE TABLE feeds
    ( url               urlType PRIMARY KEY
    );

CREATE TABLE feedMetaData
    ( feed              urlType     NOT NULL
                                    REFERENCES feeds (url)
    , title             varchar
    , link              urlType
    , description       varchar
    , language          varchar
    , copyright         varchar
    , managingEditor    emailType
    , webMaster         emailType
    , generator         varchar
    , docs              varchar
    , rating            varchar
    , ttl               integer
    , pubDate           timestamp with time zone
    , lastBuildDate     timestamp with time zone
    , pUuid             uuid        PRIMARY KEY
    , pModified         timestamp with time zone NOT NULL
    , UNIQUE (feed, pModified)
    );

/*  Aggregated feed data */
CREATE TABLE skipHours
    ( feed              urlType     NOT NULL
                                    REFERENCES feeds (url)
    , hour              smallint NOT NULL
    , UNIQUE (feed, hour)
    ); /*  Deprecated in db ver 3 */

CREATE TABLE skipDays
    ( feed              urlType     NOT NULL
                                    REFERENCES feeds (url)
    , day               weekday NOT NULL
    , UNIQUE (feed, day)
    ); /*  Deprecated in db ver 3 */

CREATE TABLE feedImages
    ( feed              urlType     NOT NULL
                                    REFERENCES feeds (url)
    , url               urlType
    , title             varchar
    , link              urlType
    , width             smallint
    , height            smallint
    , description       varchar
    , UNIQUE (feed)
    ); /*  Deprecated in db ver 3 */

CREATE TABLE clouds
    ( feed              urlType     NOT NULL
                                    REFERENCES feeds (url)
    , domain            urlType
    , port              smallint
    , path              varchar
    , registerProcedure varchar
    , protocol          varchar
    , UNIQUE (feed)
    ); /*  Deprecated in db ver 3 */

CREATE TABLE feedCategories
    ( feed              urlType     NOT NULL
                                    REFERENCES feeds (url)
    , category          varchar
    , domain            urlType
    , UNIQUE (feed, category, domain)
    );

/*  Item table */
CREATE TABLE items
    ( feed              urlType     NOT NULL
                                    REFERENCES feeds (url)
    , title             varchar
    , description       varchar
    , link              urlType
    , author            emailType
    , comments          urlType
    , guid              varchar
    , guidIsPermaLink   boolean
    , pubDate           timestamp with time zone
    , sourceUrl         urlType
    , sourceTitle       varchar
    , pUuid             uuid        PRIMARY KEY
    , pDiscovered       timestamp with time zone NOT NULL
    );

/*  Aggregated item data */
CREATE TABLE enclosures
    ( uuid              uuid        NOT NULL
                                    REFERENCES items (pUuid)
    , url               urlType
    , length            bigint
    , type              varchar
    , UNIQUE (uuid, url)
    );

CREATE TABLE itemCategories
    ( uuid              uuid        NOT NULL
                                    REFERENCES items (pUuid)
    , category          varchar
    , domain            varchar
    , UNIQUE (uuid, category, domain)
    );

/*  Users */
CREATE TABLE subscribers
    ( openid            urlType     PRIMARY KEY
    , friendlyname      varchar
    );

CREATE TABLE subscriptions
    ( subscriber        urlType     NOT NULL
                                    REFERENCES subscribers (openid)
    , feed              urlType     NOT NULL
                                    REFERENCES feeds (url)
    , PRIMARY KEY (subscriber, feed)
    );

CREATE TABLE read
    ( subscriber        urlType     NOT NULL
                                    REFERENCES subscribers (openid)
    , uuid              uuid        NOT NULL
                                    REFERENCES items (pUuid)
    , UNIQUE (subscriber, uuid)
    );

CREATE TABLE tags
    ( tag               varchar     NOT NULL
    , subscriber        urlType     NOT NULL
                                    REFERENCES subscribers (openid)
                                    ON DELETE CASCADE
                                    ON UPDATE CASCADE
    , public            boolean     NOT NULL
                                    DEFAULT false
    , PRIMARY KEY (tag, subscriber)
    );

CREATE TABLE feedTags
    ( tag               varchar     NOT NULL
    , subscriber        urlType     NOT NULL
    , feed              urlType     NOT NULL
    , FOREIGN KEY (tag, subscriber) REFERENCES tags (tag, subscriber)
    , FOREIGN KEY (subscriber, feed) REFERENCES subscriptions (subscriber, feed)
    , UNIQUE (tag, subscriber, feed)
    );

CREATE TABLE itemTags
    ( tag               varchar     NOT NULL
    , subscriber        urlType     NOT NULL
    , uuid              uuid        NOT NULL
                                    REFERENCES items (pUuid)
    , FOREIGN KEY (tag, subscriber) REFERENCES tags (tag, subscriber)
        ON DELETE CASCADE
        ON UPDATE CASCADE
    , UNIQUE (tag, subscriber, uuid)
    );

CREATE TABLE shares
    ( tag               varchar     NOT NULL
    , subscriber        urlType     NOT NULL
    , sharedwith        urlType     NOT NULL
                                    REFERENCES subscribers (openid)
    , FOREIGN KEY (tag, subscriber) REFERENCES tags (tag, subscriber)
    , UNIQUE (tag, subscriber, sharedwith)
    );

CREATE TABLE comments
    ( commenter         urlType     NOT NULL
                                    REFERENCES subscribers (openid)
    , inReplyTo         uuid        NOT NULL
                                    REFERENCES items (pUuid)
    , onTag             varchar     NOT NULL
    , onSubscriber      urlType     NOT NULL
    , timestamp         timestamp with time zone NOT NULL
    , comment           varchar     NOT NULL
    , FOREIGN KEY (onTag, onSubscriber) REFERENCES tags (tag, subscriber)
    );

/*  Aggregator metadata */
CREATE TABLE metadata
    ( key               varchar
    , value             varchar
    , UNIQUE (key, value)
    );

INSERT INTO metadata (key, value) VALUES ('version', '1');

/*  Views */
CREATE VIEW keyedItems AS
    SELECT
        *,
        COALESCE(pubDate, pDiscovered) AS pubKey,
        COALESCE(guid, TEXT(pUuid)) AS idKey
    FROM items; /*  Deprecated in db ver 3 */

CREATE VIEW latestUpdates (idKey, updated) AS
    SELECT idKey, MAX(pDiscovered)
    FROM keyedItems
    GROUP BY idKey; /*  Deprecated in db ver 3 */

/*  Items with duplicate guids filtered out */
CREATE VIEW latestItems AS
    SELECT keyedItems.*
    FROM keyedItems
    RIGHT JOIN latestUpdates
    ON  keyedItems.idKey = latestUpdates.idKey AND
        keyedItems.pDiscovered = latestUpdates.updated; /*  Deprecated in db ver 3 */

/*  Feed metadata with only the latest updates selected */
CREATE VIEW latestMetadata AS
    SELECT feedMetaData.*
    FROM feedMetaData
    RIGHT JOIN (
        SELECT feed, MAX(pModified) AS latest
        FROM feedMetaData
        GROUP BY feed
    ) AS x
    ON  feedMetaData.feed = x.feed AND
        feedMetaData.pModified = x.latest; /* Deprecated in db ver 3 */

/*  Union of itemTags and feedTags */
CREATE VIEW taggedItems (tag,subscriber,uuid) AS
    SELECT tag,subscriber,uuid
    FROM itemTags
    UNION ALL
    SELECT feedTags.tag, feedTags.subscriber, items.pUuid
    FROM feedTags
    INNER JOIN items
    ON feedTags.feed=items.feed
    LEFT JOIN itemTags
    ON  itemTags.uuid = items.pUuid AND
        itemTags.tag = feedTags.tag
    WHERE itemTags.uuid IS NULL;

/*  Views for read/unread items per feed */
CREATE VIEW readCount (subscriber, feed, read) AS
    SELECT subscriber, feed, COUNT(feed)
    FROM read
    LEFT JOIN items
    ON uuid=pUuid
    GROUP BY subscriber, feed; /*  Deprecated in db ver 3, replaced readfeedcounts */

CREATE VIEW postCount (feed, posts) AS
    SELECT feed, COUNT(feed)
    FROM items
    GROUP BY feed; /*  Deprecated in db ver 3, replaced by feedcounts */

CREATE VIEW unreadCount (subscriber, feed, unread) AS
    SELECT subscriber, postCount.feed, posts - COALESCE(read, 0)
    FROM postCount
    LEFT JOIN readCount
    ON postCount.feed=readCount.feed; /*  Deprecated in db ver 2 */

/*  Views for unread tag counts */
CREATE VIEW readTagCount (tagowner, tag, subscriber, read) AS
    SELECT  taggedItems.subscriber,
            tag,
            read.subscriber,
            COUNT(read.subscriber)
    FROM taggedItems
    INNER JOIN read
    ON taggedItems.uuid = read.uuid
    GROUP BY taggedItems.subscriber, tag, read.subscriber; /*  Deprecated in db ver 2 */

CREATE VIEW tagPostCount (tagowner, tag, posts) AS
    SELECT subscriber, tag, COUNT(tag)
    FROM taggedItems
    GROUP BY subscriber, tag; /*  Deprecated in db ver 3 */

