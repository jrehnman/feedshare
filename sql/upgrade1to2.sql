DROP VIEW 
    readTagCount,
    unreadCount;

ALTER TABLE feeds 
    ADD COLUMN hash varchar;

UPDATE metadata
    SET value='2' WHERE key='version';

