
/*
 * Fix insert statement for new feeds to be valid SQL
 */
CREATE OR REPLACE FUNCTION update_readfeedcounts() RETURNS trigger AS
'
BEGIN
    IF TG_RELNAME=''read'' THEN
        IF TG_OP=''DELETE'' OR TG_OP=''UPDATE'' THEN
            UPDATE readfeedcounts
            SET count=count-1
            WHERE
                readfeedcounts.subscriber=OLD.subscriber AND
                readfeedcounts.feed IN (
                    SELECT feed
                    FROM items
                    WHERE pUuid=OLD.uuid);
        END IF;

        IF TG_OP=''INSERT'' OR TG_OP=''UPDATE'' THEN
            UPDATE readfeedcounts
            SET count=count+1
            WHERE
                readfeedcounts.subscriber=NEW.subscriber AND
                readfeedcounts.feed IN (
                    SELECT feed
                    FROM items
                    WHERE pUuid=NEW.uuid);

            IF NOT found THEN
                INSERT INTO readfeedcounts (subscriber, feed, count)
                    SELECT 
                        NEW.subscriber, 
                        items.feed, 
                        1
                    FROM items
                    WHERE items.pUuid=NEW.uuid;
            END IF;
        END IF;
    END IF;
    RETURN NULL;
END;
' LANGUAGE plpgsql;

/*
 * Update db version
 */
UPDATE metadata
    SET value='4'
    WHERE key='version';

