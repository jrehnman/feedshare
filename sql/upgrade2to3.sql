/*
 * Drop unused views
 */
DROP VIEW
    latestItems,
    latestUpdates,
    keyedItems,
    tagPostCount,
    latestMetaData;

/*
 * Drop unused tables
 */
DROP TABLE
    skipHours,
    skipDays,
    feedImages,
    clouds;

/*
 * Update constraints on enclosures table 
 */
ALTER TABLE enclosures
    ALTER COLUMN url
    SET NOT NULL;

CREATE UNIQUE INDEX enclosures_uuid_key
    ON enclosures(uuid);

ALTER TABLE enclosures
    ADD UNIQUE USING INDEX enclosures_uuid_key; /* Allow only one enclosure per item */

ALTER TABLE enclosures
    DROP CONSTRAINT IF EXISTS enclosures_uuid_url_key;

/*
 * Alter tables to cascade on delete/update
 */
/*  subscribers (openid) */
ALTER TABLE read
    DROP CONSTRAINT IF EXISTS read_subscriber_fkey;
ALTER TABLE read
    ADD CONSTRAINT read_subscriber_fkey
    FOREIGN KEY (subscriber) REFERENCES subscribers (openid)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

ALTER TABLE shares
    DROP CONSTRAINT IF EXISTS shares_sharedwith_fkey;
ALTER TABLE shares
    ADD CONSTRAINT shares_sharedwith_fkey
    FOREIGN KEY (sharedwith) REFERENCES subscribers (openid)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

/* shares was missing a constraint on subscriber column, fixed here */
ALTER TABLE shares
    ADD CONSTRAINT shares_subscriber_fkey
    FOREIGN KEY (subscriber) REFERENCES subscribers (openid)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

ALTER TABLE subscriptions
    DROP CONSTRAINT IF EXISTS subscriptions_subscriber_fkey;
ALTER TABLE subscriptions
    ADD CONSTRAINT subscriptions_subscriber_fkey
    FOREIGN KEY (subscriber) REFERENCES subscribers (openid)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

ALTER TABLE tags
    DROP CONSTRAINT IF EXISTS tags_subscriber_fkey;
ALTER TABLE tags
    ADD CONSTRAINT tags_subscriber_fkey
    FOREIGN KEY (subscriber) REFERENCES subscribers (openid)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

/*  feeds (url) */
ALTER TABLE feedMetaData
    DROP CONSTRAINT IF EXISTS feedmetadata_feed_fkey;
ALTER TABLE feedMetaData
    ADD CONSTRAINT feedmetadata_feed_fkey
    FOREIGN KEY (feed) REFERENCES feeds (url)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

ALTER TABLE items
    DROP CONSTRAINT IF EXISTS items_feed_fkey;
ALTER TABLE items
    ADD CONSTRAINT items_feed_fkey
    FOREIGN KEY (feed) REFERENCES feeds (url)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

ALTER TABLE subscriptions
    DROP CONSTRAINT IF EXISTS subscriptions_feed_fkey;
ALTER TABLE subscriptions
    ADD CONSTRAINT subscriptions_feed_fkey
    FOREIGN KEY (feed) REFERENCES feeds (url)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

/*  items (pUuid) */
ALTER TABLE enclosures
    DROP CONSTRAINT IF EXISTS enclosures_uuid_fkey;
ALTER TABLE enclosures
    ADD CONSTRAINT enclosures_uuid_fkey
    FOREIGN KEY (uuid) REFERENCES items (pUuid)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

ALTER TABLE itemcategories
    DROP CONSTRAINT IF EXISTS itemcategories_uuid_fkey;
ALTER TABLE itemcategories
    ADD CONSTRAINT itemcategories_uuid_fkey
    FOREIGN KEY (uuid) REFERENCES items (pUuid)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

ALTER TABLE itemtags
    DROP CONSTRAINT IF EXISTS itemtags_uuid_fkey;
ALTER TABLE itemtags
    ADD CONSTRAINT itemtags_uuid_fkey
    FOREIGN KEY (uuid) REFERENCES items (pUuid)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

ALTER TABLE read
    DROP CONSTRAINT IF EXISTS read_uuid_fkey;
ALTER TABLE read
    ADD CONSTRAINT read_uuid_fkey
    FOREIGN KEY (uuid) REFERENCES items (pUuid)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

/*  subscriptions (subscriber, feed) */
ALTER TABLE feedtags
    DROP CONSTRAINT IF EXISTS feedtags_subscriber_fkey;
ALTER TABLE feedtags
    ADD CONSTRAINT feedtags_subscriber_fkey
    FOREIGN KEY (subscriber, feed) REFERENCES subscriptions (subscriber, feed)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

/*  tags (tag, subscriber) */
ALTER TABLE feedtags
    DROP CONSTRAINT IF EXISTS feedtags_tag_fkey;
ALTER TABLE feedtags
    ADD CONSTRAINT feedtags_tag_fkey
    FOREIGN KEY (tag, subscriber) REFERENCES tags (tag, subscriber)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

ALTER TABLE itemtags
    DROP CONSTRAINT IF EXISTS itemtags_tag_fkey;
ALTER TABLE itemtags
    ADD CONSTRAINT itemtags_tag_fkey
    FOREIGN KEY (tag, subscriber) REFERENCES tags (tag, subscriber)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

ALTER TABLE shares
    DROP CONSTRAINT IF EXISTS shares_tag_fkey;
ALTER TABLE shares
    ADD CONSTRAINT shares_tag_fkey
    FOREIGN KEY (tag, subscriber) REFERENCES tags (tag, subscriber)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

/*
 * Add default value for pModified column in feedMetaData
 */
ALTER TABLE feedMetaData
    ALTER COLUMN pModified
    SET DEFAULT now();

/*
 * Add default value for pDiscovered column in items
 */
ALTER TABLE items
    ALTER COLUMN pDiscovered
    SET DEFAULT now();

/*
 * Add version age column to feedmetadata
 */
ALTER TABLE feedMetaData
    ADD COLUMN pVersionAge integer NOT NULL DEFAULT 0;

CREATE SEQUENCE tmp_seq_count;
CREATE INDEX tmp_ix_feed_pmodified
    ON feedMetaData (feed, pModified DESC);

CLUSTER feedMetaData
    USING tmp_ix_feed_pmodified;

UPDATE feedMetaData
    SET pVersionAge=nextval('tmp_seq_count');

DROP SEQUENCE tmp_seq_count;
DROP INDEX tmp_ix_feed_pmodified;

UPDATE feedMetaData
    SET pVersionAge = pVersionAge - k
    FROM (
        SELECT feed, MIN(pVersionAge) AS k
        FROM feedMetaData
        GROUP BY feed) AS ageMod
    WHERE feedMetaData.feed=ageMod.feed;

ALTER TABLE feedMetaData
    DROP CONSTRAINT IF EXISTS feedmetadata_feed_pmodified_key;
ALTER TABLE feedMetaData
    ADD UNIQUE (feed, pVersionAge)
    DEFERRABLE /* Required for aging trigger to not fail uniqueness constraint */
    INITIALLY DEFERRED;

ALTER TABLE feedMetaData
    DROP COLUMN pUuid;

CREATE OR REPLACE FUNCTION age_feedmetadata() RETURNS trigger AS
'
BEGIN
    IF TG_RELNAME=''feedmetadata'' THEN
        IF TG_OP=''INSERT'' THEN
            UPDATE feedMetaData
                SET pVersionAge=pVersionAge+1
                WHERE feedMetaData.feed=NEW.feed;
        END IF;
    END IF;
    RETURN NEW;
END;
' LANGUAGE plpgsql;

CREATE TRIGGER trigger_age_feedmetadata
    BEFORE INSERT ON feedMetaData
    FOR EACH ROW
    EXECUTE PROCEDURE age_feedmetadata();

/*
 * Add sentinel column and sequence
 */
ALTER TABLE items
    ADD COLUMN pSentinel bigint;
CREATE SEQUENCE itemSentinel
    OWNED BY items.pSentinel;

/*  Add temporary index for retro-fitting values onto existing items */
CREATE INDEX tmp_ix_items_pdiscovered
    ON items (pDiscovered ASC);

CLUSTER items
    USING tmp_ix_items_pdiscovered;

/*  Retro-fit sentinel values onto existing items */
UPDATE items
    SET pSentinel = nextval('itemSentinel');

/*  Drop temporary index */
DROP INDEX tmp_ix_items_pdiscovered;

/*  Add sentinel constraints */
ALTER TABLE items
    ALTER COLUMN pSentinel
    SET NOT NULL;

ALTER TABLE items
    ALTER COLUMN pSentinel
    SET DEFAULT nextval('itemSentinel');

ALTER TABLE items
    ADD UNIQUE (pSentinel);

/*
 * Index for sorting items by pubdate and pDiscovered
 */
CREATE INDEX ix_items_pubdate_pdiscovered
    ON items
    (COALESCE(pubdate, pDiscovered), pDiscovered);

/*
 * Index for looking up items by feed
 */
CREATE INDEX ix_items_feed
    ON items
    (feed);

/*
 * Content extension
 */
ALTER TABLE items ADD COLUMN encoded varchar;

/*
 * Trigger for item counts
 */
DROP VIEW postCount;

CREATE TABLE feedcounts
    ( feed      urlType     PRIMARY KEY
                            REFERENCES feeds(url)
                            ON DELETE CASCADE
                            ON UPDATE CASCADE
    , count     bigint      NOT NULL
    );

INSERT INTO feedcounts
    SELECT feed,count(*)
    FROM items
    GROUP BY feed;

CREATE FUNCTION update_feedcounts() RETURNS trigger AS
'
BEGIN
    IF TG_RELNAME=''items'' THEN
        IF TG_OP=''DELETE'' OR TG_OP=''UPDATE'' THEN
            UPDATE feedcounts SET count=count-1 WHERE feedCounts.feed=OLD.feed;
        END IF;
        IF TG_OP=''INSERT'' OR TG_OP=''UPDATE'' THEN
            UPDATE feedcounts SET count=count+1 WHERE feedcounts.feed=NEW.feed;
            IF NOT found THEN
                INSERT INTO feedcounts
                VALUES (NEW.feed, 1);
            END IF;
        END IF;
    END IF;
    RETURN NULL;
END;
' LANGUAGE plpgsql;

CREATE TRIGGER trigger_update_feedcounts
    AFTER INSERT OR DELETE OR UPDATE ON items
    FOR EACH ROW
    EXECUTE PROCEDURE update_feedcounts();

/*
 * Trigger for number of read feed items
 */
DROP VIEW readCount;

CREATE TABLE readfeedcounts
    ( subscriber    urlType     REFERENCES subscribers(openid)
                                ON DELETE CASCADE
                                ON UPDATE CASCADE
    , feed          urlType     REFERENCES feeds(url)
                                ON DELETE CASCADE
                                ON UPDATE CASCADE
    , count         bigint      NOT NULL
    , PRIMARY KEY (subscriber, feed)
    );

INSERT INTO readfeedcounts
    SELECT subscriber, feed, count(*)
    FROM read LEFT JOIN items ON uuid=pUuid
    GROUP BY subscriber, feed;

CREATE FUNCTION update_readfeedcounts() RETURNS trigger AS
'
BEGIN
    IF TG_RELNAME=''read'' THEN
        IF TG_OP=''DELETE'' OR TG_OP=''UPDATE'' THEN
            UPDATE readfeedcounts
            SET count=count-1
            WHERE
                readfeedcounts.subscriber=OLD.subscriber AND
                readfeedcounts.feed IN (
                    SELECT feed
                    FROM items
                    WHERE pUuid=OLD.uuid);
        END IF;

        IF TG_OP=''INSERT'' OR TG_OP=''UPDATE'' THEN
            UPDATE readfeedcounts
            SET count=count+1
            WHERE
                readfeedcounts.subscriber=NEW.subscriber AND
                readfeedcounts.feed IN (
                    SELECT feed
                    FROM items
                    WHERE pUuid=NEW.uuid);

            IF NOT found THEN
                INSERT INTO readfeedcounts
                VALUES (NEW.subscriber, NEW.feed, 1);
            END IF;
        END IF;
    END IF;
    RETURN NULL;
END;
' LANGUAGE plpgsql;

CREATE TRIGGER trigger_update_readfeedcounts
    AFTER INSERT OR DELETE OR UPDATE ON read
    FOR EACH ROW
    EXECUTE PROCEDURE update_readfeedcounts();

/*
 * Create new views for read and unread counts
 */

/* Indices for speeding up counting read items per itemtag*/
CREATE INDEX ix_read_uuid 
    ON read (uuid);
CREATE INDEX ix_itemtags_uuid
    ON itemtags (uuid);

/* View of total number of items per tag */
CREATE VIEW tagcount AS
    WITH 
        feedtagcount AS (
            SELECT
                feedtags.subscriber     AS owner,
                feedtags.tag            AS tag,
                CAST(SUM(feedcounts.count) AS bigint)
                                        AS count
            FROM feedtags
            INNER JOIN feedcounts ON
                feedtags.feed           = feedcounts.feed
            GROUP BY
                feedtags.subscriber,
                feedtags.tag
        ),
        itemtagcount AS (
            SELECT
                itemtags.subscriber     AS owner,
                itemtags.tag            AS tag,
                COUNT(*)                AS count
            FROM itemtags
            GROUP BY
                itemtags.subscriber,
                itemtags.tag
        ),
        duplicatecount AS (
            SELECT
                itemtags.subscriber     AS owner,
                itemtags.tag            AS tag,
                COUNT(*)                AS count
            FROM itemtags
            LEFT JOIN items ON
                itemtags.uuid           = items.pUuid
            INNER JOIN feedtags ON
                itemtags.subscriber     = feedtags.subscriber AND
                itemtags.tag            = feedtags.tag AND
                items.feed              = feedtags.feed
            GROUP BY
                itemtags.subscriber,
                itemtags.tag
        )
    SELECT
        tags.subscriber                 AS owner,
        tags.tag                        AS tag,
        COALESCE(feedtagcount.count, 0) +
        COALESCE(itemtagcount.count, 0) -
        COALESCE(duplicatecount.count, 0)
                                        AS count
    FROM tags
    LEFT JOIN feedtagcount ON
        tags.subscriber                 = feedtagcount.owner AND
        tags.tag                        = feedtagcount.tag
    LEFT JOIN itemtagcount ON
        tags.subscriber                 = itemtagcount.owner AND
        tags.tag                        = itemtagcount.tag
    LEFT JOIN duplicatecount ON
        tags.subscriber                 = duplicatecount.owner AND
        tags.tag                        = duplicatecount.tag;

/* View of number of read items per tag and subscriber */
CREATE OR REPLACE VIEW tagreadcount AS
    WITH
        feedtagread AS (
            SELECT
                feedtags.subscriber     AS owner,
                feedtags.tag            AS tag,
                readfeedcounts.subscriber AS subscriber,
                CAST(SUM(readfeedcounts.count) AS bigint)
                                        AS count
            FROM feedtags
            INNER JOIN readfeedcounts ON
                feedtags.feed           = readfeedcounts.feed
            GROUP BY
                feedtags.subscriber,
                feedtags.tag,
                readfeedcounts.subscriber
        ),
        itemtagread AS (
            SELECT
                itemtags.subscriber     AS owner,
                itemtags.tag            AS tag,
                read.subscriber         AS subscriber,
                COUNT(read.uuid)        AS count
            FROM itemtags
            LEFT JOIN read ON
                itemtags.uuid           = read.uuid
            GROUP BY
                itemtags.subscriber,
                itemtags.tag,
                read.subscriber
        ),
        duplicateread AS (
            SELECT
                feedtags.subscriber     AS owner,
                feedtags.tag            AS tag,
                read.subscriber         AS subscriber,
                COUNT(*)                AS count
            FROM itemtags
                LEFT JOIN items ON
                    itemtags.uuid=items.pUuid
                INNER JOIN feedtags ON
                    itemtags.subscriber = feedtags.subscriber AND
                    itemtags.tag        = feedtags.tag AND
                    items.feed          = feedtags.feed
                INNER JOIN read ON
                    itemtags.uuid       = read.uuid
            GROUP BY
                feedtags.subscriber,
                feedtags.tag,
                read.subscriber
        )
    SELECT
        tags.subscriber                 AS owner,
        tags.tag                        AS tag,
        subscribers.openid              AS subscriber,
        COALESCE(feedtagread.count, 0) +
        COALESCE(itemtagread.count, 0) -
        COALESCE(duplicateread.count, 0) 
                                        AS count
    FROM tags
    JOIN subscribers ON
        TRUE
    LEFT JOIN feedtagread ON
        tags.subscriber                 = feedtagread.owner AND
        tags.tag                        = feedtagread.tag AND
        subscribers.openid              = feedtagread.subscriber
    LEFT JOIN itemtagread ON
        tags.subscriber                 = itemtagread.owner AND
        tags.tag                        = itemtagread.tag AND
        subscribers.openid              = itemtagread.subscriber
    LEFT JOIN duplicateread ON
        tags.subscriber                 = duplicateread.owner AND
        tags.tag                        = duplicateread.tag AND
        subscribers.openid              = duplicateread.subscriber;

/* View of number of unread items per tag and subscriber */
CREATE OR REPLACE VIEW tagunreadcount AS
    SELECT
        tagreadcount.owner              AS owner,
        tagreadcount.tag                AS tag,
        tagreadcount.subscriber         AS subscriber,
        tagcount.count -
        tagreadcount.count              AS unread
    FROM tagreadcount
    LEFT JOIN tagcount ON
        tagreadcount.owner              = tagcount.owner AND
        tagreadcount.tag                = tagcount.tag;

/*
 * Update db version
 */
UPDATE metadata
    SET value='3'
    WHERE key='version';

