
from atomschema import atomSchema, atomtransform
from database import WrappedCursor
from datetime import datetime
from feedparser import FeedError, XmlParseError, FeedFormatError
from instafetch import instafetch
from psycopg2 import InterfaceError, DatabaseError
from rssschema import rssSchema
from utils import intersperse, repeat, current_exception
import confighelper
import curlutil
import database
import databaseif as dbif
import feedparser
import hashlib
import re
import sys
import threadpool
import time
import uuid


## Default config file path
defaultconfigfile = 'scraper.json'

## Default config
config = \
    { 'database': 'feedshare'
    , 'dbuser': 'feedshare'
    , 'threads': 1
    , 'period': 0
    , 'updatelimit': 10
    }

## Pattern for identifying instagram urls
instagram_pattern = re.compile(r'https://(?:www\.)?instagram.com/(?P<username>[^/]+)/?')


class FetchError(Exception):
    def __init__(self, url):
        Exception.__init__(self, f'Could not fetch {url}')
        self.url = url

class SchemaError(Exception):
    def __init__(self, xml):
        Exception.__init__(self, f'Could not determine feed schema from context "{xml.tag}"')
        self.tag = xml.tag

class FeedUnchanged(Exception):
    pass


def sqlargumentlookup(column, values):
    return values[column] if \
        values                          and \
        (column in values)              and \
        (values[column] is not None)    and \
        (values[column] != '')          else None

def sqlconditions(table, columns, values):
    result = []
    for c in columns:
        argument = sqlargumentlookup(c, values)
        comparison = 'IS' if argument is None else '='
        condition = f'{table}.{c} {comparison} %s'

        result.append((condition, argument))

    return result

def fst(xs):
    return xs[0]

def snd(xs):
    return xs[1]

def propertiesNotIn(schema, exclude):
    return [p.name for p in schema.properties if p.name not in exclude]

def insertInto(cursor, schema, data):
    columns = [p.name for p in schema.properties if sqlargumentlookup(p.name, data)]
    columnsstr = intersperse(columns, ', ')
    valuesstr = intersperse(['%s' for c in columns], ', ')
    query = f'INSERT INTO {schema.name} ({columnsstr}) VALUES ({valuesstr});'

    parameters = [sqlargumentlookup(c, data) for c in columns]
    cursor.execute(query, tuple(parameters))

def insertitem(cursor, item):
    itemSchema = rssSchema.children['items']
    enclosureSchema = itemSchema.children['enclosures']
    categoriesSchema = itemSchema.children['itemCategories']

    insertInto(cursor, itemSchema, item)
    if item['enclosures'] and item['enclosures']['url']:
        insertInto(cursor, enclosureSchema, item['enclosures'])
    for category in item['itemCategories']:
        insertInto(cursor, categoriesSchema, category)

def processitem(cursor, url, item):
    # Update item with synthetic attributes
    item['feed'] = url
    item['pUuid'] = uuid.uuid4()

    if item['enclosures']:
        item['enclosures']['uuid'] = item['pUuid']
        try:
            _ = int(item['enclosures']['length'], 0)
        except:
            del item['enclosures']['length']

    for category in item['itemCategories']:
        category['uuid'] = item['pUuid']

    # Schema for item
    itemSchema = rssSchema.children['items']
    enclosuresSchema = itemSchema.children['enclosures']

    # SQL query for finding existing rows
    itemkeys = ['feed', 'title', 'description', 'encoded', 'link', 'author', 'guid']
    enclosurekeys = ['url']

    conditions = \
        sqlconditions('items', itemkeys, item) + \
        sqlconditions('enclosures', enclosurekeys, item['enclosures'])

    comparisons = list(map(fst, conditions))
    arguments = tuple(map(snd, conditions))

    query = 'SELECT COUNT(*) > 0 \
        FROM items \
        LEFT JOIN enclosures \
        ON items.pUuid = enclosures.uuid \
        WHERE {0};'.format(intersperse(comparisons, ' AND '))

    # Find if item exists or not
    cursor.execute(query, arguments)
    (rowexists, ) = cursor.fetchone()

    if not rowexists:
        # Row doesn't exist, insert
        insertitem(cursor, item)
    else:
        # Update ignored fields
        pass

    return (not rowexists, item['guid'])

def redactitems(cursor, guid, limit):
    #query = '\
    #    SELECT pDiscovered, pUuid \
    #    FROM items \
    #    WHERE guid=%s';

    #cursor.execute(query, (guid, ))
    #items = cursor.fetchall()

    #(keep, discard) = redact.redact(items, limit)
    #discarded_uuids = map(snd, discard)

    #query = '\
    #    DELETE FROM items \
    #    WHERE pUuid = ANY(%s);';

    #cursor.execute(query, (discarded_uuids, ))
    #return len(discarded_uuids)
    pass

def fetchfeed(url):
    try:
        # TODO Check for HTTP 3xx redirect headers and update database
        match = instagram_pattern.search(url)
        if match:
            feedsrc = instafetch.instafetch(match.group('username')).encode()
        else:
            feedsrc = curlutil.curlget(url, useragent='FeedShare')
        return feedsrc
    except:
        raise FetchError(url)

def determineschema(xml):
    if xml.tag == 'rss':
        return (rssSchema, None)
    elif xml.tag == '{http://www.w3.org/2005/Atom}feed':
        return (atomSchema, atomtransform)
    else:
        return (None, None)

def parsefeed(url, storedhash):
    feedsrc = fetchfeed(url)
    feedhash = hashlib.new('md5', feedsrc).hexdigest()

    if storedhash and (storedhash == feedhash):
        raise FeedUnchanged()

    xml = feedparser.parsexml(feedsrc)
    schema, transform = determineschema(xml)
    if not schema:
        raise SchemaError(xml)

    feeddata = feedparser.parsetag(xml, schema, log)
    channel = transform(feeddata) if transform else feeddata

    return (feedhash, channel)

def updatefeed(cursor, url):
    storedhash = dbif.getstoredhash(cursor, url)
    feedhash, channel = parsefeed(url, storedhash)
    dbif.storehash(cursor, url, feedhash)

    # Update synthetic attributes
    channel['feed'] = url
    channel['pVersionAge'] = 0

    keys = propertiesNotIn(rssSchema, ['pModified', 'ttl', 'pubDate', 'lastBuildDate'])
    conditions = sqlconditions('feedMetaData', keys, channel)
    comparisons = list(map(fst, conditions))
    arguments = tuple(map(snd, conditions))

    query = f'''SELECT COUNT(*) > 0
        FROM feedMetaData \
        WHERE {intersperse(comparisons, ' AND ')};'''

    # Find if feedmetadata already exists
    cursor.execute(query, arguments)
    (rowexists, ) = cursor.fetchone()

    if not rowexists:
        insertInto(cursor, rssSchema, channel)
        log(f'New metadata found for {url}')
        # Redact intermediate updates
    else:
        # Update ignored fields ('ttl', 'pubDate', 'lastBuildDate')
        pass

    # Insert items
    new_items = 0
    redactable = set()
    for item in channel['items']:
        try:
            with WrappedCursor(cursor.connection) as c:
                new, guid = processitem(c, url, item)
                if new:
                    new_items += 1
                    if guid:
                        redactable.add(guid)
        except:
            log(f'Exception inserting item from {url}, {current_exception()}')

    if new_items > 0:
        log(f'{new_items} new items found for {url}')

    for guid in redactable:
        # Redact updates to scraper limit
        with WrappedCursor(cursor.connection) as c:
            redactitems(c, guid, config['updatelimit'])

def log(x):
    msg = f'[{datetime.now()}] {x}'
    print(msg)

def pollfeed(url, connection):
    try:
        log(f'Hit: {url}')
        with WrappedCursor(connection) as c:
            updatefeed(c, url)
    except FeedUnchanged:
        log(f'Feed "{url}" unchanged')
    except FetchError as e:
        log(f'Fetch error from {url}, {e.args[0]}')
    except XmlParseError as e:
        log(f'Could not parse xml in feed from {url}, {e.args[0]}')
    except FeedFormatError as e:
        log(f'Error in feed from {url} at line {e.line}, {e.args[0]}')
    except SchemaError as e:
        log(f'Could not determine feed from context "{e.tag}" in "{url}"')
    except:
        log(f'Unexpected error in {url}')
        raise

def pollfeeds():
    log("Start scrape")
    db = database.connect(config['database'], config['dbuser'])
    try:
        pool = threadpool.Pool(config['threads'])

        c = db.cursor()
        c.execute("SELECT url FROM feeds;")
        urls = [url for (url, ) in c]
        c.close()

        pool.map(pollfeed, urls, connection=db)
        pool.close()
    except:
        pool.terminate()
        raise
    finally:
        pool.join()
        db.close()

    log("End scrape")

def scrapeforever():
    while(True):
        pollfeeds()
        time.sleep(config['period'])

def loadconfig(configfile = defaultconfigfile):
    """Shorthand for loading configuration from (optionally specified from command line) file"""
    confighelper.read_json_config(config, configfile)

def main():
    # Read command line options
    argparser = confighelper.config_argsparser(defaultconfigfile)
    opts = argparser.parse_args()
    
    # Write config
    if opts.write_default_config:
        confighelper.write_default_config(config, opts.config_file)
        return

    # Read config
    loadconfig(configfile = opts.config_file)

    try:
        if config['period'] > 0:
            scrapeforever()
        else:
            pollfeeds()

    except KeyboardInterrupt:
        print('Ctrl-C')

if __name__ == '__main__':
    main()

