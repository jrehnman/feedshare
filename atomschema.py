
from schema import schemaRoot

def atomtransform(parseresult):
    result = {}

    result['title']   = parseresult['title']
    result['description'] = parseresult['subtitle']

    for link in parseresult['link']:
        if (link['rel'] == None) or (link['rel'] == 'alternate'):
            result['link'] = link['href']
            break

    result['pubDate'] = parseresult['updated']
    result['copyright'] = parseresult['rights']
    result['generator'] = parseresult['generator']

    result['items'] = []
    for entry in parseresult['entry']:
        result['items'].append(transformentry(entry))

    result['feedCategories'] = []
    for category in parseresult['category']:
        result['feedCategories'].append(transformcategory(category))

    return result

def transformentry(entry):
    result = {}

    result['guid'] = entry['id']
    result['title'] = entry['title']
    result['pubDate'] = entry['published'] if entry['published'] else entry['updated']

    for link in entry['link']:
        if (link['rel'] == None) or (link['rel'] == 'alternate'):
            result['link'] = link['href']
            break
    else:
        result['link'] = None

    result['author'] = transformauthor(entry['author'])
    result['description'] = entry['summary']
    result['encoded'] = entry['content']['value'] if entry['content'] else None

    result['itemCategories'] = []
    for category in entry['category']:
        result['itemCategories'].append(transformcategory(category))

    for link in entry['link']:
        if link['rel'] == 'enclosure':
            result['enclosures'] = transformenclosure(link)
            break
    else:
        result['enclosures'] = None

    return result

def transformauthor(author):
    if author is None:
        return None
    elif author['name'] and author['email']:
        return "{} {}".format(author['name'], author['email'])
    elif author['name']:
        return author['name']
    elif author['email']:
        return author['email']
    else:
        return None

def transformcategory(category):
    result = {}

    if category['label']:
        result['category'] = category['label']
    elif category['term']:
        result['category'] = category['term']

    result['domain'] = None

    return result

def transformenclosure(link):
    return \
        { 'url': link['href'] if link['href'] else None
        , 'length': link['length'] if link['length'] else None
        , 'type': link['type'] if link['type'] else None
        }

atomSchema = \
    schemaRoot('feed', ns='atom', namespaces={'atom': 'http://www.w3.org/2005/Atom'}) \
        .defaultnamespace('atom') \
            .synthetic('feed') \
            .textprop('id') \
            .textprop('title') \
            .textprop('subtitle') \
            .child('link', many=True) \
                .attribute('rel') \
                .attribute('href') \
            .end() \
            .textprop('updated') \
            .textprop('rights') \
            .textprop('generator') \
            .textprop('id') \
            .child('category', many=True) \
                .attribute('term') \
                .attribute('scheme') \
                .attribute('label') \
            .end() \
            .child('entry', many=True) \
                .textprop('id') \
                .textprop('title') \
                .textprop('published') \
                .textprop('updated') \
                .child('link', many=True) \
                    .attribute('href') \
                    .attribute('rel') \
                    .attribute('type') \
                    .attribute('hreflang') \
                    .attribute('title') \
                    .attribute('length') \
                .end() \
                .prop('link', many=True, xpath='atom:link/@href') \
                .child('author') \
                    .textprop('name') \
                    .textprop('email') \
                .end() \
                .textprop('summary') \
                .child('content') \
                    .attribute('type') \
                    .attribute('src') \
                    .prop('value', xpath='.') \
                .end() \
                .child('category', many=True) \
                    .attribute('term') \
                    .attribute('scheme') \
                    .attribute('label') \
                .end() \
            .end()
