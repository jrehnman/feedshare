
from lxml import etree


def parseOpml(text):
    xml = etree.fromstring(text)
    feeds = {}
    for node in xml.xpath('//outline[@type="rss"]'):
        try:
            url = node.attrib['xmlUrl']
            name = node.attrib['text']
            tags = node.xpath('ancestor::outline/@text')
            if url in feeds:
                feeds[url]['title'].add(name)
                feeds[url]['tags'].update(tags)
            else:
                feeds[url] = \
                    { 'title': set([name])
                    , 'tags': set(tags)
                    }
        except KeyError:
            pass

    return [ \
        { 'url': k
        , 'title': list(feeds[k]['title']) \
        , 'tags': list(feeds[k]['tags']) \
        } for k in feeds ]

