
def addNs(ns, element):
    return "{}:{}".format(ns, element) if ns else element

def schemaRoot(name, xpath=None, namespaces={}, ns=None):
    return Schema(name, many=False, required=True, xpath=xpath if xpath else "/{}".format(addNs(ns, name)), namespaces=namespaces)

class Schema:
    def __init__(self, name, many=False, required=False, xpath=None, namespaces={}, ns=None, synthetic=False):
        self.name = name
        self.xpath = xpath if xpath else addNs(ns, name)
        self.many = many
        self.required = required

        self.properties = []
        self.children = {}

        self.namespaces = namespaces
        self.defaultns = None

        self.issynthetic = synthetic
        self.__parent = None

    def prop(self, name, many=False, required=False, xpath=None, ns=None, synthetic=False):
        p = Schema(name, many, required, xpath, self.namespaces, ns if ns else self.defaultns, synthetic)
        self.properties.append(p)
        return self

    def textprop(self, name, many=False, required=False, ns=None):
        return self.prop(name, many, required, xpath="{}/text()".format(self.__addns(ns, name)))

    def attribute(self, name, required=False):
        return self.prop(name, required, xpath='@'+name)

    def synthetic(self, name):
        return self.prop(name, synthetic=True)

    def child(self, name, many=False, required=False, xpath=None, ns=None):
        s = Schema(name, many, required, xpath, self.namespaces, ns if ns else self.defaultns)
        s.defaultnamespace(self.defaultns)
        s.__parent = self
        self.children[name] = s
        return s

    def namespace(self, **ns):
        self.namespaces.update(ns)
        return self

    def defaultnamespace(self, ns):
        self.defaultns = ns
        return self

    def __addns(self, ns, name):
        return addNs(ns if ns else self.defaultns, name)

    def end(self):
        return self.__parent

    def __repr__(self):
        return self.name

