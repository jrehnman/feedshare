
from SimpleSecureHTTPServer import SecureHTTPServer
from JsonRpcRequestHandler import JsonRpcRequestHandler

import argparse
import confighelper
import database
from database import WrappedCursor
import databaseif as dbif
import json
from jsonrpc import JsonRpcError
import openid.consumer.consumer as openid
import opml
import os.path
import scraper


## Config file path
defaultconfigfile = 'config.json'

## Default config
config = \
    { 'pemfile':    './server.pem'
    , 'listen':     '0.0.0.0'
    , 'port':       443
    , 'database':   'feedshare'
    , 'dbuser':     'feedshare'
    , 'extra-headers': []
    }

## OpenID store. Force stateless mode for now.
openidstore = None

## Per-session storage
g_sessions = {}

## Database connection
db = None

#
# JSON-RPC errors
#
e_already_subscribed_to_feed    = (1001, 'User already subscribed to feed')
e_unknown_session               = (1002, 'Unknown session')
e_not_logged_in                 = (1003, 'User not logged in')
e_already_registered            = (1004, 'OpenID already registered')
e_unrecognized_openid           = (1005, 'OpenID could not be resolved')
e_unknown_user                  = (1006, 'No such user')
e_already_tagged                = (1007, 'Tag already attached to object')
e_parse_error                   = (1008, 'Parse error')
e_already_shared                = (1009, 'Tag already shared with user')
e_no_such_tag                   = (1011, 'No such tag')
e_no_such_item                  = (1012, 'No such feed item')
e_insufficient_privileges       = (1013, 'Insufficient privileges')


class RssRequestHandler(JsonRpcRequestHandler):
    def setup(self):
        JsonRpcRequestHandler.setup(self)
        self.webroot = './webroot'
        self.redirects = { '/': '/reader.html' }
        self.scripts = { '/openidcallback': self.script_openidcallback }
        self.sessions = g_sessions
        self.extra_headers = config['extra-headers']

    def json_closesession(self):
        sessionid = self.getsessionid()
        if not sessionid in self.sessions:
            raise JsonRpcError(*e_unknown_session)

        del self.sessions[sessionid]
        return True

    def json_parseopml(self, data):
        try:
            feeds = opml.parseOpml(data.encode('UTF-8'))
            return feeds
        except Exception as e:
            raise JsonRpcError(*e_parse_error)

    def json_subscribe(self, url):
        user = self.json_getauth()

        # TODO Trim whitspace
        # TODO Check if URL/feed is valid
        
        with WrappedCursor(db) as c:
            if not dbif.subscribe(c, user, url):
                raise JsonRpcError(*e_already_subscribed_to_feed)

            if not dbif.feeddataexists(c, url):
                scraper.updatefeed(c, url)

        return True

    def json_unsubscribe(self, url):
        user = self.json_getauth()
        with WrappedCursor(db) as c:
            dbif.unsubscribe(c, user, url)

    def json_getsubscriptions(self):
        user = self.json_getauth()
        with WrappedCursor(db) as c:
            return dbif.getsubscriptions(c, user)

    def json_getsentinel(self):
        with WrappedCursor(db) as c:
            return dbif.getsentinel(c)

    def json_getsharedtags(self):
        user = self.json_getauth()
        with WrappedCursor(db) as c:
            return dbif.getsharedtags(c, user)

    def json_getfeeditems(self, feed, first, last, onlyunread, ascending, refsentinel):
        user = self.json_getauth()
        with WrappedCursor(db) as c:
            items = dbif.feeditems(c, user, feed, first, last, onlyunread, ascending, refsentinel)
            return items

    def json_gettaggeditems(self, tag, first, last, onlyunread, ascending, refsentinel):
        user = self.json_getauth()
        with WrappedCursor(db) as c:
            items = dbif.taggeditems(c, user, tag, user, first, last, onlyunread, ascending, refsentinel)
            return items

    def json_getshareditems(self, tagowner, tag, first, last, onlyunread, ascending, refsentinel):
        user = self.json_getauth()
        with WrappedCursor(db) as c:
            items = dbif.taggeditems(c, tagowner, tag, user, first, last, onlyunread, ascending, refsentinel)
            return items

    def json_markread(self, postid, isread):
        user = self.json_getauth()
        try:
            with WrappedCursor(db) as c:
                if isread:
                    dbif.markitemread(c, user, postid)
                else:
                    dbif.markitemunread(c, user, postid)

        except dbif.NoSuchItemError:
            raise JsonRpcError(*e_no_such_item)

    def json_markfeedread(self, feed, refsentinel):
        user = self.json_getauth()
        with WrappedCursor(db) as c:
            dbif.markfeedread(c, user, feed, refsentinel)

    def json_marktagread(self, tag, refsentinel):
        user = self.json_getauth()
        with WrappedCursor(db) as c:
            dbif.marktagread(c, user, tag, user, refsentinel)

    def json_marksharedread(self, tagowner, tag, refsentinel):
        user = self.json_getauth()
        with WrappedCursor(db) as c:
            dbif.marktagread(c, user, tag, tagowner, refsentinel)

    def json_tagitem(self, postid, tagstring):
        user = self.json_getauth()
        # TODO Don'tRepeatYourself        
        tags = [tag.strip() for tag in tagstring.split(',')]
        with WrappedCursor(db) as c:
            return dbif.tagitem(c, user, postid, tags)

    def json_gettags(self, postid):
        user = self.json_getauth()
        with WrappedCursor(db) as c:
            tags = dbif.getuseritemtags(c, user, postid)
            return tags

    def json_tagfeed(self, feedurl, tagstring):
        user = self.json_getauth()
        # TODO Don'tRepeatYourself        
        tags = [tag.strip() for tag in tagstring.split(',')]
        with WrappedCursor(db) as c:
            return dbif.tagfeed(c, user, feedurl, tags)

    def json_untagfeed(self, feedurl, tag):
        user = self.json_getauth()
        with WrappedCursor(db) as c:
            dbif.untagfeed(c, user, feedurl, tag)

    def json_getfeedtags(self, feedurl):
        user = self.json_getauth()
        with WrappedCursor(db) as c:
            tags = dbif.getuserfeedtags(c, user, feedurl)
            return tags

    def json_getmytags(self):
        user = self.json_getauth()
        with WrappedCursor(db) as c:
            return dbif.getallusertags(c, user)

    def json_deletetag(self, tag):
        user = self.json_getauth()
        with WrappedCursor(db) as c:
            dbif.deletetag(c, user, tag)

    def json_getsharedwith(self, tag):
        user = self.json_getauth()
        with WrappedCursor(db) as c:
            return dbif.gettagsharedwith(c, user, tag)

    def json_sharetag(self, tag, sharewith):
        try:
            user = self.json_getauth()
            with WrappedCursor(db) as c:
                dbif.sharetag(c, user, tag, sharewith)
            return self.json_getsharedwith(tag)
        except dbif.NoSuchTagError:
            raise JsonRpcError(*e_no_such_tag)
        except dbif.NoSuchUserError:
            raise JsonRpcError(*e_unknown_user)
        except dbif.AlreadySharedError:
            raise JsonRpcError(*e_already_shared)

    def json_unsharetag(self, tag, sharedwith):
        user = self.json_getauth()
        with WrappedCursor(db) as c:
            dbif.unsharetag(c, user, tag, sharedwith)

    def json_feedstats(self):
        with WrappedCursor(db) as c:
            feeds = dbif.feedstats(c)
            return feeds

    def json_userstats(self):
        if self.json_isadmin():
            with WrappedCursor(db) as c:
                users = dbif.userstats(c)
                return users
        else:
            raise JsonRpcError(*e_insufficient_privileges)

    def json_purge(self, feedurl):
        if self.json_isadmin():
            with WrappedCursor(db) as c:
                dbif.purgefeed(c, feedurl)
        else:
            raise JsonRpcError(*e_insufficient_privileges)

    def json_isadmin(self):
        user = self.json_getauth()
        with WrappedCursor(db) as c:
            admin = dbif.userisadmin(c, user)
            return admin
    
    def json_register(self, openidurl, redirecthost):
        # Perform first part of OpenID authentication so any problem
        # resolving/parsing OpenID URL is raised before being added
        # to database
        openid_redirect = self.__json_beginauthenticate(openidurl, redirecthost)

        with WrappedCursor(db) as c:
            if not dbif.adduser(c, openidurl):
                raise JsonRpcError(*e_already_registered)
        
        return openid_redirect

    def json_authenticate(self, openidurl, redirecthost):
        # Validate OpenID by performing first part of authentication
        # before checking database if user exists
        openid_redirect = self.__json_beginauthenticate(openidurl, redirecthost)

        with WrappedCursor(db) as c:
            if not dbif.userexists(c, openidurl):
                raise JsonRpcError(*e_unknown_user)

        return openid_redirect

    def __json_beginauthenticate(self, openidurl, redirecthost):
        s = self.getsession()
        s['openid'] = openidurl
        s['authenticated'] = False
        s['redirecthost'] = redirecthost

        c = openid.Consumer(s, openidstore)
        try:
            authrequest = c.begin(openidurl)
        except openid.DiscoveryFailure as e:
            raise JsonRpcError(e_unrecognized_openid[0], e.message)

        redirecturl = authrequest.redirectURL(
            redirecthost,
            redirecthost + '/openidcallback')
        return redirecturl

    def json_getauth(self):
        s = self.getsession()
        if not 'openid' in s or not 'authenticated' in s or not s['authenticated']:
            raise JsonRpcError(*e_not_logged_in)

        user = s['openid']
        return user

    def script_openidcallback(self, reqtype, data, query):
        # urlparse module returns a list of values for every query key
        # openid expects a single value per key.
        for k in query.keys():
            query[k] = query[k][0]

        try:
            s = self.getsession()
            c = openid.Consumer(s, openidstore)
            result = c.complete(query, s['redirecthost'] + self.path)

            if result.status == openid.SUCCESS:
                # Authentication OK, redirect to reader
                redirecturl = s['redirecthost']
                s['authenticated'] = True
                self.send_response(303)
                self.send_header('Location', redirecturl)
                self.end_headers()
                html = f'''<html><body>
                    Authentication successful, continue at
                    <a href="{redirecturl}">{redirecturl}</a>
                    </body></html>'''
                self.wfile.writelines([html.encode()])
            else:
                # Authentication Failed
                self.send_response(200)
                self.end_headers()
                reason = getattr(result, 'message', None)
                html = '<html><body> \
                    Authentication failed: {0} (Reason: {1})\
                    </body></html>'.format(result.status, reason)
                self.wfile.writelines([html])
        except KeyError:
            # No active session
            self.send_response(200)
            self.end_headers()
            html = '<html><body> \
                No active openid authentication found \
                </body></html>'
            self.wfile.writelines([html])

def main(HandlerClass = RssRequestHandler,
         ServerClass = SecureHTTPServer):
    
    # Read command line options
    argparser = confighelper.config_argsparser(defaultconfigfile)
    opts = argparser.parse_args()
    
    # Write config
    if opts.write_default_config:
        confighelper.write_default_config(config, opts.config_file)
        return

    # Read config
    confighelper.read_json_config(config, opts.config_file)

    # Connect to database
    global db
    db = database.connect(config['database'], config['dbuser'])

    # Set up server
    server_address = (config['listen'], config['port'])
    httpd = ServerClass(server_address, HandlerClass, config['pemfile'])
    sa = httpd.socket.getsockname()
    print('Serving HTTPS on', sa[0], 'port', sa[1], '...')
    
    # Serve
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        print('\nStopping on keyboard interrupt')
    finally:
        httpd.shutdown()

if __name__ == '__main__':
    main()

