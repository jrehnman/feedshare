
import sys


def intersperse(strs, separator):
    if len(strs) > 0:
        result = strs[0]
        for s in strs[1:]:
            result += "{0}{1}".format(separator, s)
        return result
    else:
        return ""

def repeat(x, n):
    return [x for y in xrange(n)]

def current_exception():
    (t, v, tb) = sys.exc_info()
    return v

