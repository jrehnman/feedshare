
function callbackgenerator(obj, f)
{
    return function()
    {
        if (obj.readyState == 4)
        {
            f(obj)
        }
    };
}

function createServer()
{
    var sendRequest = function(method, url, data, f)
    {
        var xmlhttp = new XMLHttpRequest();
        var callback = callbackgenerator(xmlhttp, f);
        xmlhttp.open(method, url, true);
        xmlhttp.onreadystatechange = callback;
        xmlhttp.send(data);
    };
    
    var o = {};
    o.getRequest = function(url, data, f)
    {
        sendRequest("GET", url, data, f);
    };
    o.postRequest = function(url, data, f)
    {
        sendRequest("POST", url, data, f);
    };
    return o;
}
        
