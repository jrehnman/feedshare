/*
 * Functions that do not rely on state
 */

function setelementvisible(element, visible)
{
    element.style.display = visible ? "inline" : "none"
}

function togglefold(element)
{
    var isvisible = element.style.display != "none"
    element.style.display = isvisible ? "none" : "block"
}

function stringcomparator(a, b)
{
    if (a > b)
    {
        return 1
    }
    else if (a < b)
    {
        return -1
    }
    else
    {
        return 0
    }
}

function iteratorToArray(it)
{
    var arr = []
    var n = it.iterateNext()
    while(n)
    {
        arr.push(n)
        n = it.iterateNext()
    }
    return arr
}

function postprocessitem(post)
{
    if (!post["title"])
    {
        if (post["link"])
        {
            post["title"] = post["link"]
        }
        else
        {
            post["title"] = post["pDiscovered"]
        }
    }
    post["content"] = post["encoded"] ? post["encoded"] : post["description"]

    if (post["enclosure-url"] && post["enclosure-type"])
    {
        var enctype = post["enclosure-type"]
        if (enctype.startsWith("image/"))
        {
            post["enclosure-image"] = post["enclosure-url"]
        }
        else if (enctype.startsWith("audio/"))
        {
            post["enclosure-audio"] = post["enclosure-url"]
        }
        else if (enctype.startsWith("video/"))
        {
            post["enclosure-video"] = post["enclosure-url"]
        }
    }
}

function postprocesshtml(item, dom)
{
    // Open link in new tabs/windows
    var hrefs = iteratorToArray(dom.evaluate("//a[@href]", dom))
    for (var k in hrefs)
    {
        hrefs[k].target = "_blank"
    }

    // Enable playback controls on media
    var media = iteratorToArray(dom.evaluate("//video|//audio", dom))
    for (var k in media)
    {
        media[k].controls = true
        media[k].autoplay = false
        if (media[k].preload = "auto")
        {
            media[k].preload = "metadata"
        }
    }

    for (var k = 0; k < dom.images.length; k++)
    {
        var img = dom.images[k]
        
        // Chrome don't load mixed content with srcset attribute present?
        if ("srcset" in img.attributes)
        {
            img.attributes.removeNamedItem("srcset")
        }

        // Rewrite relative URLs
        if ("src" in img.attributes)
        {
            var uri = document.createElement('a')
            uri.href = img.attributes.getNamedItem('src').value

            if (uri.origin == document.location.origin)
            {
                var feeduri = document.createElement('a')
                feeduri.href = item["feed"]

                uri.protocol = feeduri.protocol
                uri.host = feeduri.host
                if (feeduri.port)
                {
                    uri.port = feeduri.port
                }
                img.src = uri.href
            }
        }

        // Constrain image sizes
        img.style.maxWidth = "100%"
        img.style.width = "auto"
        img.style.height = "auto"
    }

    // Change iframes to secure URL. Cross-content iframes
    // won't load anyway.
    var iframes = iteratorToArray(dom.evaluate("//iframe", dom))
    for (var k in iframes)
    {
        var iframe = iframes[k]
        var srcattr = iframe.attributes.getNamedItem('src')
        if (srcattr)
        {
            var iframesrc = srcattr.value
            var uri = document.createElement('a')
            uri.href = iframesrc

            if (uri.protocol == 'http:')
            {
                uri.protocol = 'https:'
            }

            iframe.src = uri.href
        }
    }

    // Remove scripts
    for (var k = 0; k < dom.scripts.length; k++)
    {
        var script = dom.scripts[k]
        script.parentElement.removeChild(script)
    }

    return dom
}

