
function substitute(template, parameters, rtemplates)
{
    /*
     * Syntax:
     * %foo%            - Substituted by value 'foo' from dict 'parameters'
     * $foo#bar$        - Substituted by applying value(s) 'bar' from dict 'parameters' to template 'foo'
     * $foo@bar$        - Substituted by applying 'parameters' with key 'bar' replaced by each value of list 'bar' to template 'foo'
     * $foo[@|#]bar+sep$ - As above, with each substitution separated by 'sep'
     */

    var parameterpattern = /%([a-zA-Z_-]+)%|\$([a-zA-Z_-]*)(@|#)([a-zA-Z_-]+)(?:\+([^\$]*))?\$/g
    var replacefn = function(str, p, fn, calltype, q, sep)
    {
        if (p && parameters && (parameters[p] != null))
        {
            return parameters[p]
        }
        else if (fn && q && (parameters[q] != null) && rtemplates && rtemplates[fn])
        {
            var rt = rtemplates[fn]
            var subarg = parameters[q]
            var pcopy = copydict(parameters)
            if (Array.isArray(subarg))
            {
                var result = ""
                for (var i = 0; i < subarg.length; i++)
                {
                    if (calltype == "@")
                    {
                        pcopy[q] = subarg[i]
                    }

                    result += substitute(rt, calltype=="#" ? subarg[i] : pcopy, rtemplates)
                    if (sep && i != subarg.length - 1)
                    {
                        result += sep
                    }
                }
                return result
            }
            else
            {
                return substitute(rt, calltype=="#" ? subarg : pcopy, rtemplates)
            }
        }
        else
        {
            return ""
        }
    }

    var replacement = template.replace(parameterpattern, replacefn)
    return replacement
}

function copydict(d)
{
    var copy = {}
    var keys = Object.keys(d)
    for (var i = 0; i < keys.length; i++)
    {
        var key = keys[i]
        copy[key] = d[key]
    }
    return copy
}

function applytemplates(template, data, rtemplates, tf)
{
    var result = ""
    for (var i = 0; i < data.length; i++)
    {
        result += substitute(template, tf ? tf(data[i]) : data[i], rtemplates)
    }
    return result
}

