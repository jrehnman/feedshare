
/**
 * Constants
 */
var post_fetch_count = 20

/**
 * JSON-RPC interface to server
 */
var rpc = new jsonrpc("/request")

/*
 * GUI elements
 */
var guielements = {}
var guikeys =
    [ "subscriptions"
    , "sharedtags"
    , "feedinfo"
    , "posts"
    , "postspinner"
    , "user"
    , "feedurl"
    , "showonlyunread"
    , "logoutbox"
    , "tagadministration"
    , "opmlfileselect"
    , "opmlspinner"
    , "opmllist"
    , "opmlsubscribe"
    , "opmldiscard"
    ]

/*
 * Templates
 */
var templateuri = 'templates.html'
var templates = {}
var templateroot = "templates"

/**
 * List of available feeds
 */
var g_feedlist = []

/**
 * Dictionary of available feed, keyed by url
 */
var g_feeddict = {}

/**
 * List of shared tags
 */
var g_sharedtags = []

/**
 * List of tags
 */
var g_tags = []

/**
 * Tag information lookup by tagname
 */
var g_tagindex = {}

/**
 * Feed that is active and how many posts from feed that is currently
 * displayed.
 */
var g_currentposts = {}

/**
 * List of feeds to import from OPML
 */
var g_opmlimport = {}

/**
 * List of anchors and url, for keyboard navigation
 */
var g_navdata = []

/**
 * Index of currently active navigation anchor
 */
var g_currentanchor = -1


/*
 * Utilities
 */

function init()
{
    loadgui()
    rpc.call(showuser, { 1003: gotoauthenticate }, "getauth")
    loadtemplates()
}

function loadgui()
{
    for (var i = 0; i < guikeys.length; i++)
    {
        var key = guikeys[i]
        guielements[key] = document.getElementById(key)
    }
}

function loadtemplates()
{
    var s = createServer()
    s.getRequest(templateuri, null, ontemplates)

}

function ontemplates(obj)
{
    var parser  = new DOMParser()
    var templatedom = parser.parseFromString(obj.responseText, "text/html")
    var templatecontainer = templatedom.getElementById(templateroot)
    for (var i = 0; i < templatecontainer.children.length; i++)
    {
        var template = templatecontainer.children[i]
        if (template.id)
        {
            templates[template.id] = template.innerHTML
        }
    }
    
    refresh()
}

function refresh()
{
    reqmytags()
    reqfeeds()
    reqsharedtags()
}

function Posts(feedsource, first, last, limit, readcount, sentinel)
{
    this.feedsource = feedsource
    this.first = first
    this.last = last
    this.limit = limit
    this.readcount = readcount
    this.sentinel = sentinel
}


/*
 * Authentication
 */

function showuser(user)
{
    guielements["user"].innerHTML = user
}

function gotoauthenticate()
{
    document.location = "authenticate.html"
}

function showsignedout()
{
    guielements["logoutbox"].style.visibility = "visible"
}

function logout()
{
    rpc.call(gotoauthenticate, { 1002: gotoauthenticate }, "closesession")
}


/*
 * Feed related functionality
 */

function subscribe()
{
    var url = guielements["feedurl"].value
    var errors =
        { 1001: alreadysubscribed
        , 1003: showsignedout
        }
    rpc.call(subscriptionok, errors, "subscribe", url)
}

function alreadysubscribed()
{
    alert("Already in subscription list")
}

function subscriptionok()
{
    guielements["feedurl"].value = ""
    reqfeeds()
}

function unsubscribe(url, title)
{
    if (confirm("Unsubscribe from " + title + "?"))
    {
        var errors = { 1003: showsignedout }
        rpc.call(reqfeeds, errors, "unsubscribe", url)
    }
}

function reqfeeds()
{
    rpc.call(recvfeeds, { 1003: showsignedout }, "getsubscriptions")
}

function recvfeeds(subscriptions)
{
    g_feedlist = subscriptions
    g_feeddict = {}

    for (var i = 0; i < g_feedlist.length; i++)
    {
        var feed = g_feedlist[i]
        var hasunread = feed["unread"] > 0
        feed["unreadlabel"] = hasunread ? feed["unread"] : null

        if (!feed["title"])
        {
            feed["title"] = feed["feed"]
        }

        g_feeddict[feed['feed']] = feed
    }

    if (g_feedlist.length == 0)
    {
        guielements["subscriptions"].innerHTML = "No feeds"
    }
    else
    {
        var slist = sortbytag(subscriptions)
        guielements["subscriptions"].innerHTML = substitute(
            templates["subscriptionstemplate"],
            slist,
            templates)
    }
}

function sortbytag(subscriptions)
{
    var tagmap = {}
    var untagged = []
    for (var i = 0; i < subscriptions.length; i++)
    {
        var feed = subscriptions[i]
        var feedurl = feed["feed"]
        for (var j = 0; j < feed.tags.length; j++)
        {
            var tag = feed.tags[j]
            if (!(tag in tagmap))
            {
                tagmap[tag] = {}
            }
            tagmap[tag][feedurl] = feed
        }
        if (feed.tags.length == 0)
        {
            untagged.push(feed)
        }
    }

    var result = { "tags": [], "untagged": untagged }

    var feedsort = function(a, b)
    {
        if (a["unread"] != b["unread"])
        {
            return b["unread"] - a["unread"]
        }
        else
        {
            return stringcomparator(a["title"], b["title"])
        }
    }
    for (var tagname in tagmap)
    {
        var tag = g_tagindex[tagname]
        var feeds = []
        for (var feed in tagmap[tagname])
        {
            feeds.push(tagmap[tagname][feed])
        }
        feeds.sort(feedsort)

        result["tags"].push({"tag": tag, "feeds": feeds})
    }
    result["untagged"].sort(feedsort)

    var tagsort = function(a, b)
    {
        return stringcomparator(a["tag"]["tag"], b["tag"]["tag"])
    }
    result["tags"].sort(tagsort)
    return result
}

/*
 * Shared tags
 */

function reqsharedtags()
{
    var errors = { 1003: showsignedout }
    rpc.call(recvsharedtags, errors, "getsharedtags")
}

function recvsharedtags(sharedtags)
{
    g_sharedtags = sharedtags;

    for (var i = 0; i < g_sharedtags.length; i++)
    {
        var tag = g_sharedtags[i]
        var hasunread = tag["unread"] > 0
        tag["unreadlabel"] = hasunread ? tag["unread"] : null
    }

    if (g_sharedtags.length == 0)
    {
        guielements["shraedtags"].innerHTML = "No shared tags"
    }
    else
    {
        guielements["sharedtags"].innerHTML = applytemplates(
            templates["sharedtagtemplate"],
            g_sharedtags)
    }
}

/*
 * Post fetching
 */

function reqposts(feedurl, onlyunread)
{
    guielements["posts"].innerHTML = ""
    var feedsource =
        { "type": "feed"
        , "feed": feedurl
        , "onlyunread": onlyunread
        }
    guielements["feedinfo"].innerHTML = substitute(
        templates["feedinfotemplate"],
        g_feeddict[feedurl],
        templates)
    g_currentposts = new Posts(feedsource, 0, 0, post_fetch_count, 0, null)
    rpc.call(getfirst, {}, "getsentinel")
}

function reqtaggedposts(tag, onlyunread)
{
    guielements["feedinfo"].innerHTML = "<b>Tag: <a href=\"#\" onclick=\"reqtaggedposts('" + tag + "', getonlyunread())\">" + tag + "</a></b>"
    guielements["posts"].innerHTML = ""
    var feedsource =
        { "type": "tag"
        , "tag": tag
        , "onlyunread": onlyunread
        }
    g_currentposts = new Posts(feedsource, 0, 0, post_fetch_count, 0, null)
    rpc.call(getfirst, {}, "getsentinel")
}

function reqsharedposts(sharer, tag, onlyunread)
{
    guielements["feedinfo"].innerHTML = "<b>" + sharer + "/" + tag + "</b>"
    guielements["feedinfo"].innerHTML = ""
    guielements["posts"].innerHTML = ""
    var feedsource =
        { "type": "sharedtag"
        , "sharer": sharer
        , "tag": tag
        , "onlyunread": onlyunread
        }

    g_currentposts = new Posts(feedsource, 0, 0, post_fetch_count, 0, null)
    rpc.call(getfirst, {}, "getsentinel")
}

function getfirst(sentinel)
{
    g_currentposts.sentinel = sentinel
    g_navdata = []
    g_currentanchor = -1
    getmore()
}

function getonlyunread()
{
    var ischecked = guielements["showonlyunread"].checked
    return ischecked
}

function getmore(callback)
{
    if (g_currentposts && g_currentposts.feedsource)
    {
        var onlyunread = g_currentposts.feedsource.onlyunread
        var offset = g_currentposts.last - (onlyunread ? g_currentposts.readcount : 0)

        var f = function(items) {
            recvposts(items)
            if (callback)
            {
                callback()
            }
        }

        switch (g_currentposts.feedsource.type)
        {
            case "feed":
                rpc.call(
                    f,
                    { 1003: showsignedout },
                    "getfeeditems",
                    g_currentposts.feedsource.feed,
                    offset,
                    offset + g_currentposts.limit,
                    onlyunread,
                    false,
                    g_currentposts.sentinel)
                break;

            case "tag":
                rpc.call(
                    f,
                    { 1003: showsignedout },
                    "gettaggeditems",
                    g_currentposts.feedsource.tag,
                    offset,
                    offset + g_currentposts.limit,
                    onlyunread,
                    false,
                    g_currentposts.sentinel)
                break;

            case "sharedtag":
                rpc.call(
                    f,
                    { 1003: showsignedout },
                    "getshareditems",
                    g_currentposts.feedsource.sharer,
                    g_currentposts.feedsource.tag,
                    offset,
                    offset + g_currentposts.limit,
                    onlyunread,
                    false,
                    g_currentposts.sentinel)
                break;

            default:
                return;
        }
        setelementvisible(guielements["postspinner"], true)
    }
}

function recvposts(items)
{
    var parser
    try
    {
        parser  = new DOMParser()
    }
    catch(e)
    {
    }

    g_currentposts.last += items.length

    for (var i = 0; i < items.length; i++)
    {
        g_navdata.push({"pUuid": items[i]["pUuid"], "link": items[i]["link"]})

        if (items[i]["read"])
        {
            items[i]["readchecked"] = "checked"
            g_currentposts.readcount += 1
        }
        else
        {
            items[i]["readchecked"] = "dummy"
        }

        postprocessitem(items[i])

        if (parser)
        {
            var domtree = parser.parseFromString(items[i]["content"], "text/html")
            if (domtree)
            {
                domtree = postprocesshtml(items[i], domtree)

                var body = iteratorToArray(domtree.evaluate("//body[1]", domtree))
                items[i]["content"] = body[0].innerHTML
            }
        }
    }

    if (g_currentposts.last == 0)
    {
        guielements["posts"].innerHTML = "No posts"
    }
    else
    {
        guielements["posts"].innerHTML += applytemplates(
            templates["posttemplate"],
            items,
            templates)
    }
    for (var i = 0; i < items.length; i++)
    {
        document.getElementById('post-'+items[i]["pUuid"]).className = 'postdiv ' + (items[i]["read"] ? 'postdiv-read' : 'postdiv-unread')
    }

    setelementvisible(guielements["postspinner"], false)
}

function markread(postid, read)
{
    var ignore = function() {}
    var errors =
        { 1003: showsignedout
        , 1012: ignore // Ignore no such item error
        }

    g_currentposts.readcount += read ? 1 : -1
    rpc.call(refreshfeedsources, errors, "markread", postid, read)

    postdiv = document.getElementById('post-' + postid)
    postdiv.className = read ? 'postdiv postdiv-read' : 'postdiv postdiv-unread'
}

function unreadTag(tag)
{
    for (var i = 0; i < g_tags.length; i++)
    {
        if (g_tags[i].tag == tag)
        {
            return g_tags[i].unread
        }
    }
    return -1
}

function unreadFeed(feed)
{
    for (var i = 0; i < g_feedlist.length; i++)
    {
        if (g_feedlist[i].feed == feed)
        {
            return g_feedlist[i].unread
        }
    }
    return -1
}

function markallread()
{
    var go = true
    var visibleposts = g_currentposts.last - g_currentposts.first
    var visible_unread = visibleposts - g_currentposts.readcount

    switch (g_currentposts.feedsource.type)
    {
        case "feed":
            var feed = g_currentposts.feedsource.feed
            var unread = unreadFeed(feed)
            var hidden_unread = unread - visible_unread

            if (hidden_unread > 0)
            {
                go = confirm("Mark " + hidden_unread + " not shown items as read?")
            }
            if (go)
            {
                g_currentposts.readcount = visibleposts
                var errors = { 1003: showsignedout }
                rpc.call(refreshfeedsources, errors, "markfeedread", feed, g_currentposts.sentinel)
            }
            break

        case "tag":
            var tag = g_currentposts.feedsource.tag
            var unread = unreadTag(tag)
            var hidden_unread = unread - visible_unread

            if (unreadTag(tag) > visible_unread)
            {
                go = confirm("Mark " + hidden_unread + " not shown items as read?")
            }
            if (go)
            {
                g_currentposts.readcount = visibleposts
                var errors = { 1003: showsignedout }
                rpc.call(refreshfeedsources, errors, "marktagread", tag, g_currentposts.sentinel)
            }
            break

        case "shared":
            // Yah, do the mass marking confirmation here too mkay?
            g_currentposts.readcount = visibleposts
            var tagowner = g_currentposts.feedsource.sharer
            var tag = g_currentposts.feedsource.tag
            var errors = { 1003: showsignedout }
            rpc.call(refreshfeedsources, errors, "marksharedread", tagowner, tag, g_currentposts.sentinel)
            break

        default:
            break
    }

    if (go)
    {
        var divs = document.getElementsByTagName('div')
        p = /(^|\s)postdiv($|\s)/
        for (var i = 0; i < divs.length; i++)
        {
            if (p.test(divs[i].className))
            {
                divs[i].className = "postdiv postdiv-read"
            }
        }
    }
}

function refreshfeedsources()
{
    switch (g_currentposts.feedsource.type)
    {
        case "feed":
        case "tag":
            reqmytags()
            reqfeeds()
            break;
        case "sharedtag":
            reqsharedtags()
            break;
        default:
            break;
    }
}


/*
 * Tag related functions
 */

function tag(postid, tagname)
{
    if (!tagname)
    {
        tagname = prompt("Tag: ")
    }
    if (tagname)
    {
        var callback = reqposttags.bind(undefined, postid)
        var errors =
            { 1003: showsignedout
            , 1007: tagalreadyapplied
            }
        rpc.call(callback, errors, "tagitem", postid, tagname)
    }
}

function tagalreadyapplied()
{
    alert("That tag is already applied")
}

function reqposttags(postid)
{
    var errors = { 1003: showsignedout }
    var callback = recvposttags.bind(undefined, postid)
    rpc.call(callback, errors, "gettags", postid)
}

function recvposttags(postid, tags)
{
    var tagspan = document.getElementById("tags-" + postid)
    if (tagspan)
    {
        tagspan.innerHTML = tags
    }
}

function tagfeed(feedurl, tagname)
{
    if (!tagname)
    {
        tagname = prompt("Tag: ")
    }
    if (tagname)
    {
        var errors = { 1003: showsignedout }
        rpc.call(reqfeeds, errors, "tagfeed", feedurl, tagname)
    }
}

function untagfeed(feedurl, tagname)
{
    var errors = { 1003: showsignedout }
    rpc.call(reqfeeds, errors, "untagfeed", feedurl, tagname)
}

function reqmytags()
{
    var errors = { 1003: showsignedout }
    rpc.call(recvmytags, errors, "getmytags")
}

function recvmytags(tags)
{
    g_tags = tags;

    for (var i = 0; i < g_tags.length; i++)
    {
        var tag = g_tags[i]
        g_tagindex[tag['tag']] = tag
        tag["unreadlabel"] = tag["unread"] ? tag["unread"] : null
    }

    function tagsort(a, b)
    {
        return stringcomparator(a["tag"], b["tag"])
    }

    g_tags.sort(tagsort)

    guielements["tagadministration"].innerHTML = substitute(
        "$tagadmintemplate#values$",
        {"values": g_tags },
        templates)
}

function sharetag(tag)
{
    var sharewith = prompt('User')
    if (sharewith)
    {
        var errors =
            { 1003: showsignedout
            , 1009: function() {} // Ignore already shared
            , 1010: function() { alert("Unknown user") }
            , 1011: function() {} // Ignore unknown tag
            }
        rpc.call(reqmytags, errors, "sharetag", tag, sharewith)
    }
}

function unshare(tag, user)
{
    var errors = { 1003: showsignedout }
    rpc.call(reqmytags, errors, "unsharetag", tag, user)
}

function deletetag(tag)
{
    if (confirm("Tag will be removed from all feeds and items as well as unshared with all subscribers. Continue removing \"" + tag + "\"?"))
    {
        var errors = { 1003: showsignedout }
        rpc.call(reqmytags, errors, "deletetag", tag)
    }
}

/*
 * OPML import related functions
 */

function parseopml()
{
    var file = guielements["opmlfileselect"].files[0]
    if (file)
    {
        var fr = new FileReader()
        fr.onloadend = function()
        {
            var opml = fr.result;
            var errors = { 1008: parseerror }
            rpc.call(populateopmllist, errors, "parseopml", opml)
            setelementvisible(guielements["opmlspinner"], true)
        }
        fr.readAsText(file)
    }
}

function populateopmllist(subscriptions)
{
    g_opmlimport = {}

    var existingfeeds = {}
    for (var i = 0; i < g_feedlist.length; i++)
    {
        var url = g_feedlist[i].feed
        existingfeeds[url] = true
    }

    for (var i = 0; i < subscriptions.length; i++)
    {
        var exists = subscriptions[i].url in existingfeeds
        subscriptions[i]["disabled"] = exists ? "disabled" : "dummy"
    }

    if (subscriptions.length == 0)
    {
        guielements["opmllist"].innerHTML = "No feeds found"
    }
    else
    {
        guielements["opmllist"].innerHTML = applytemplates(
            templates["opmltemplate"],
            subscriptions)
    }

    setelementvisible(guielements["opmlsubscribe"], true)
    setelementvisible(guielements["opmldiscard"], true)
    setelementvisible(guielements["opmlspinner"], false)
}

function parseerror()
{
    guielements["opmllist"].innerHTML = "Couldn't read OPML"
    setelementvisible(guielements["opmlspinner"], false)
}

function includeopmlsubscription(url, include)
{
    if (!include)
    {
        delete g_opmlimport[url]
    }
    else
    {
        g_opmlimport[url] = true
    }
}

function subscribeopml()
{
    var errors =
        { 1001: alreadysubscribed
        , 1003: showsignedout
        }

    for (url in g_opmlimport)
    {
        var tags = document.getElementById("importtags-" + url).value
        var callback = tagopml.bind(undefined, url, tags)
        rpc.call(callback, errors, "subscribe", url)
    }
    reqfeeds()
    discardopml()
}

function tagopml(url, tags)
{
    var ignore = function() {}
    var errors =
        { 1003: ignore // Ignore signed out for batch tagging
        , 1007: ignore // Ignore duplicate tags
        }
    rpc.call(ignore, errors, "tagfeed", url, tags)
}

function discardopml()
{
    g_opmlimport = {}
    guielements["opmllist"].innerHTML = ""
    setelementvisible(guielements["opmlsubscribe"], false)
    setelementvisible(guielements["opmldiscard"], false)
}

/*
 * Keyboard navigation
 */
function navigate()
{
    switch(event.keyCode)
    {
        case 110: // N
            var f = function() {
                g_currentanchor = Math.min(g_currentanchor + 1, g_navdata.length - 1)
                document.location = "#nav" + g_navdata[g_currentanchor]["pUuid"]
            }

            if (g_currentanchor+1 < g_navdata.length)
            {
                f()
            }
            else
            {
                getmore(f)
            }
            break;
        case 112: // P
            g_currentanchor = Math.max(g_currentanchor - 1, 0)
            document.location = "#nav" + g_navdata[g_currentanchor]["pUuid"]
            break;
        case 109: // M
            if (g_currentanchor < g_navdata.length)
            {
                markread(g_navdata[g_currentanchor]["pUuid"], true)
            }
            break;
        case 117: // U
            if (g_currentanchor < g_navdata.length)
            {
                markread(g_navdata[g_currentanchor]["pUuid"], false)
            }
            break;
        case 111: // O
            if (g_currentanchor < g_navdata.length)
            {
                window.open(g_navdata[g_currentanchor]["link"], '_blank')
            }
            break;
        case 103: // G
            getmore()
            break;
        case 97:  // A
            tag(g_navdata[g_currentanchor]["pUuid"])
            break;
        default:
            break;
    }
}

function setcurrentanchor(uuid)
{
    for (var i = 0; i < g_navdata.length; i++)
    {
        if (g_navdata[i]["pUuid"] == uuid)
        {
            g_currentanchor = i;
            break;
        }
    }
}
