

function dummyerrhandler()
{
    var log = function()
    {
        try
        {
            if (console && console.debug)
            {
                console.debug.apply(console, arguments)
            }
        }
        catch(err)
        {
        }
    }

    this.unexpectederror = function() { log.apply(this, arguments) }
    this.invalidjson = function() { log.apply(this, arguments) }
    this.parseerror = function() { log.apply(this, arguments) }
}

function jsonrpc(url, errhandler)
{
    this.jsonid = 0
    this.errhandler = errhandler ? errhandler : new dummyerrhandler()

    this.call = function(f, e, method)
    {
        var argarray = Array.prototype.slice.call(arguments);
        var params = (argarray.length > 3) ? argarray.slice(3) : []
    
        var serv = createServer()
        var req = 
            { "jsonrpc": "2.0"
            , "method": method
            , "params": params
            , "id": this.jsonid++
            } 

        var thisjsonrpc = this
        var callback = function(obj)
        {
            var jsonresponse
            try 
            {
                jsonresponse = JSON.parse(obj.responseText)
                if (!jsonresponse)
                {
                    throw "Empty response"
                }
            }
            catch(err)
            {
                thisjsonrpc.errhandler.parseerror(err)
                return
            }

            if (!jsonresponse["error"])
            {
                f(jsonresponse["result"])
            }
            else if (jsonresponse["error"] && !jsonresponse["result"])
            {
                var error = jsonresponse["error"]
                var code = error["code"]
                var message = error["message"]
                if ((code != null) && (message != null))
                {
                    if (code in e)
                    {
                        e[code](message)
                    }
                    else
                    {
                        thisjsonrpc.errhandler.unexpectederror(code, message)
                    }
                }
                else
                {
                    thisjsonrpc.errhandler.invalidjson(jsonresponse)
                }
            }
            else
            {
                thisjsonrpc.errhandler.invalidjson(jsonresponse)
            }
        }

        serv.postRequest(url, JSON.stringify(req), callback);
    }
}



