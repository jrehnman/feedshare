
import socket
from http.server import HTTPServer
from socketserver import ThreadingMixIn
import ssl


class SecureHTTPServer(ThreadingMixIn, HTTPServer):
    def __init__(self, server_address, HandlerClass, fpem):
        HTTPServer.__init__(self, server_address, HandlerClass)
        s = socket.socket(self.address_family, self.socket_type)
        self.socket = ssl.wrap_socket(s, certfile=fpem, server_side=True)
        self.server_bind()
        self.server_activate()

    def shutdown(self):
        HTTPServer.shutdown(self)
        self.socket.close()

