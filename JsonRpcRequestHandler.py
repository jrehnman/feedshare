
import json
import jsonrpc
import os
import re
import shutil
import time
import urllib.parse
import uuid

from http.server import BaseHTTPRequestHandler


class JsonRpcRequestHandler(BaseHTTPRequestHandler):
    def setup(self):
        BaseHTTPRequestHandler.setup(self)
        self.__sessionid = None
        self.redirects = {}
        self.scripts = {}
        self.sessions = {}
        self.jsonrpcurl = '/request'
        self.webroot = './webroot'
        self.extra_headers = []

    def do_GET(self):
        urlcomponents = urllib.parse.urlparse(self.path)
        mappedpath = self.redirects.get(urlcomponents.path, urlcomponents.path)
        webrootpath = self.getpage(mappedpath)
        if webrootpath:
            self.send_response(200)
            self.end_headers()

            with open(webrootpath, 'rb') as f:
                shutil.copyfileobj(f, self.wfile)
        elif urlcomponents.path in self.scripts:
            method = self.scripts[urlcomponents.path]
            query = urllib.parse.parse_qs(urlcomponents.query)
            method("GET", "", query)
        else:
            self.send_error(404)

    def do_POST(self):
        if self.path == self.jsonrpcurl:
            jsonstr = self.getdata()
            self.log_message('%s', jsonstr)

            response = jsonrpc.jsonrpc(jsonstr, self)

            jsonstr = json.dumps(response, ensure_ascii=False, default=jsonrpc.extended_json_transform)
            jsonbytes = jsonstr.encode()

            self.send_response(200)
            self.send_header('Content-Encoding', 'identity')
            self.send_header('Content-Type', 'application/json')
            self.send_header('Content-Length', len(jsonbytes))
            self.send_header('Connection', 'close')
            self.end_headers()
            self.wfile.write(jsonbytes)

        elif self.path in self.scripts:
            method = self.scripts[self.path]
            data = self.getdata()
            query = {}
            method("POST", data, query)

        else:
            self.send_error(404)

    def getdata(self):
        data = ""
        contentlengthstr = self.headers.get('content-length')
        if contentlengthstr:
            contentlength = int(contentlengthstr)
            data = self.rfile.read(contentlength)
        return data

    def getsession(self):
        sessionid = self.getsessionid()
        s = self.sessions[sessionid]
        return s

    def getsessioncookie(self):
        cookiestr = self.headers.get('cookie')
        if cookiestr:
            match = re.match(".*sessionid=([^;]*)", cookiestr)
            if match:
               sessionid = match.group(1)
               if sessionid in self.sessions:
                   return sessionid

        return None

    def sendsessioncookie(self, sessionid):
        maxclientage = 3600 * 24 * 365
        self.send_header("Set-Cookie", "sessionid={0}; Max-Age={1}; Secure; HttpOnly".format(sessionid, maxclientage))
        self.sessions[sessionid] = {}

    def getsessionid(self):
        if not self.__sessionid:
            self.__sessionid = self.getsessioncookie()
            if not self.__sessionid:
                self.__sessionid = str(uuid.uuid4())

        return self.__sessionid

    def end_headers(self):
        if not self.getsessioncookie():
            sessionid = self.getsessionid()
            self.sendsessioncookie(sessionid)

        for hdr in self.extra_headers:
            self.send_header(hdr[0], hdr[1])

        BaseHTTPRequestHandler.end_headers(self)

    def getpage(self, path):
        p = os.path.join(self.webroot, path[1:])
        if os.path.isfile(p) and \
                os.path.exists(p) and \
                issubpath(self.webroot, p):
            return p
        else:
            return None

def issubpath(parentpath, subpath):
    p1 = os.path.realpath(parentpath)
    p2 = os.path.realpath(subpath)
    stem = os.path.commonprefix([p1, p2])
    return os.path.samefile(parentpath, stem)

