
import threading
import queue

class Pool:
    def __init__(self, maxthreads):
        self.__maxthreads = maxthreads
        self.__poolresource = threading.BoundedSemaphore(self.__maxthreads)
        self.__closethread = None

    def map(self, f, xs, **kwargs):
        spawnqueue = queue.Queue(1)
        livethreads = queue.Queue()

        def runf(x):
            try:
                f(x, **kwargs)
            finally:
                self.__poolresource.release()

        def spawner():
            while True:
                (ok, x) = spawnqueue.get()
                if ok:
                    t = threading.Thread(group=None, target=runf, args=(x,))
                    self.__poolresource.acquire()
                    t.start()
                    livethreads.put(t)
                else:
                    break

            livethreads.put(None)

        def consumer():
            while True:
                t = livethreads.get()
                if t:
                    t.join()
                else:
                    break

        try:
            consume_thread = threading.Thread(group=None, target=consumer)
            consume_thread.start()

            spawn_thread = threading.Thread(group=None, target=spawner)
            spawn_thread.start()

            for x in xs:
                spawnqueue.put((True, x))

        finally:
            spawnqueue.put((False, None))
            spawn_thread.join()
            consume_thread.join()

    def close(self):
        def consumer():
            for n in range(self.__maxthreads):
                self.__poolresource.acquire()

        if self.__closethread is None:
            self.__closethread = threading.Thread(group=None, target=consumer)
            self.__closethread.start()

    def terminate(self):
        self.close()

    def join(self):
        self.__closethread.join()

